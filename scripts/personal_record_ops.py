#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function
import codecs
import csv
import getopt
import json
import os
import shlex
import sys

import django.contrib.auth
from django.core.exceptions import ObjectDoesNotExist

import user_mgmt.models
import user_mgmt.user_operations


def update_user_dictionary(user_id, context_name, user_dict):
    """
    Update the dictionary of the element in the PersonalRecord table for the
    given user_id and context_id

    :param user_id: User id to search for the entry in the DB to update
    :param context_name: Context to use in the search
    :param user_dict: Dictionary object to store in dictionary field
    :return: Nothing. Changes reflected in the DB

    If anything goes wrong or the parameters are given the wrong values,
    the function raises an exception.
    """

    if user_id is None or context_name is None or context_name == '' or \
       user_dict is None or user_dict == '':
        raise Exception('One of the parameters, at least is empty')

    element = user_mgmt.models.PersonalRecord.objects.get(
        user=user_id, context__name=context_name
    )

    element.dictionary = json.dumps(user_dict)
    element.save()


def insert_user_as_context_member(user_email, context_name, debug=False):
    """

    :param user_email: String with an email address
    :param context_name: String with a context name
    :param debug: Debug flag
    :return: Inserts a new user member in the given context
    """

    # Get the user object given the email
    user = user_mgmt.user_operations.user_lookup_by_email(user_email)

    # If the user is not found, bomb out to see why
    if user is None:
        raise Exception('User ' + user_email + ' not found.')

    try:
        user.contexts.get(name=context_name)
    except ObjectDoesNotExist:
        ctx = user_mgmt.models.Context.objects.get(name=context_name)
        ctx.members.add(user.id)
        ctx.save()
        if debug:
            print('Inserted', user.id, '-', context_name, file=sys.stderr)
    else:
        if debug:
            print(user.id, 'already in context', context_name, file=sys.stderr)


def remove_fields(field_name, field_value, field_list, context_name, debug):
    """
    :param field_name: Field to identify the user to process
    :param field_value: Value of the field to identify the user to process
    :param field_list: List of fields to remove from dictionary
    :param context_name: String with the name of the context
    :param debug: Verbosity level
    :return: Nothing. Results reflected in the database
    """

    # Get the PersonalRecords row for the given pair, but differentiate if
    # the request is for a single user or a set of records
    if field_name == 'user_id' or field_name == 'email':
        user_records = [get_user_record(field_name, field_value, context_name)]
    else:
        user_records = get_record_list(field_name, field_value, context_name)

    if not user_records:
        print('No users match keys/value', ','.join(field_name), end=' ',
              file=sys.stderr)
        print('not found', file=sys.stderr)
        return

    # At this point, remove the values from the dictionary
    if debug:
        print('Fields to remove:', str(field_list), file=sys.stderr)

    # Loop over all the records obtained
    modified = 0  # Count the number of records modified.
    for user_data in user_records:
        # Get the dictionary and the user id from the record
        user_dict = json.loads(user_data.dictionary)
        user_id = user_data.user.id

        # Loop over the pairs and update the content
        update_db = False
        for field_name in field_list:
            # Remove field
            if user_dict.pop(field_name, None) is None:
                print('Key', field_name, 'not present in user', end=' ',
                      file=sys.stderr)
                print(user_id, file=sys.stderr)
            else:
                # DB needs to be updated
                update_db = True
                modified += 1

        if update_db:
            if debug:
                print('Updating user with id', user_id, file=sys.stderr)

            update_user_dictionary(user_id, context_name, user_dict)
    print(modified, 'rows modified.', file=sys.stderr)


def insert_users_from_csv_file(file_name, lookup_column, header_mark, debug):
    """
    Function that given a CSV file, parses its content, selects the value in
    the column with name "lookup_column" and creates a user with that name.
    :param file_name: CSV file to process.
    :param lookup_column: column to obtain the user name
    :param header_mark: String to detect the header
    :param debug: Print more information
    """

    # Open the file for reading
    file_in = codecs.open(file_name, 'rU')
    dialect = csv.Sniffer().sniff(file_in.read(1024))
    file_in.seek(0)
    data_in = csv.reader(file_in, dialect)

    # Loop over the lines
    line_number = 0
    header_detected = False
    lookup_idx = -1
    for data in data_in:
        # Count the line number to flag anomalies
        line_number += 1

        # If mark has not been detected yet
        if not header_detected:
            if header_mark is not None and header_mark not in data:
                # Mark is given, but line does not match, skip it
                continue

            # At this point either the mark has been detected, or no mark has
            # been given, in which case, still take it as detected
            header_detected = True

            # If the column to look up the entry is not there, terminate
            if lookup_column in data:
                lookup_idx = data.index(lookup_column)
            else:
                print('Column', lookup_column, 'not found.', file=sys.stderr)
                print('Columns:', data, file=sys.stderr)
                sys.exit(1)

            if debug:
                print('Columns:', data, file=sys.stderr)
                print('Lookup idx:', lookup_idx, file=sys.stderr)

            # Proceed with the following lines
            continue

        # Safety check. If something went wrong when the CSV file was
        # exported, it is very likely that the string #REF! is present. If
        # so, notify and stop.
        if '#REF!' in data:
            print('Line', line_number, 'contains incorrect data',
                  file=sys.stderr)
            sys.exit(1)

        # At this point we are processing a data line

        # If the number of fields does not match then number of columns Flag!
        if lookup_idx >= len(data):
            print('Mismatched line', line_number, 'skipping', file=sys.stderr)
            continue

        # Special cases to detect garbage at the end of the scripts (some)
        if data[0].startswith('Usage of this data is guided by'):
            continue
        if data[0].startswith('[Generated by '):
            continue

        # Create the new user with the given user id
        msg = user_mgmt.user_operations.activate_account_with_email(
            data[lookup_idx]
        )
        # If msg is a string, something went wrong.
        if type(msg) is str:
            print(msg)

    return


def perform_update_or_set_record(field_name,
                                 field_value,
                                 context_name,
                                 pair_list=None,
                                 debug=False,
                                 insert_only=False,
                                 single=False):
    """
    Function that given a (name, value) key, it locates that row in the
    database and either inserts or updates (insert_only) the list of pairs (
    name, value) given. There is no result, the changes are reflected in the
    database.

    :param field_name: value to search for the entry to update in the DB
    :param field_value: Field to search for the user
    :param context_name: string encoding the context name
    :param pair_list: List of (name, value) to insert/update
    :param debug: Control verbosity
    :param insert_only: Only allow insertion of non-existing names.
    :param single: Only allow one single element to update
    :return: Nothing. Changes reflected in the DB
    """

    # Get the PersonalRecords row for the given pair, but differentiate if
    # the request is for a single user or a set of records
    if field_name == 'user_id' or field_name == 'email':
        user_records = [get_user_record(field_name, field_value, context_name)]
    else:
        user_records = get_record_list(field_name, field_value, context_name)

    # If no data has been selected, notify and terminate
    if not user_records:
        return

    # If no pairs are given, no operation, simply dump content
    if not pair_list:
        return

    # If a single element is required and there are more than one, Exception.
    if single and len(user_records) > 1:
        print('Update/Set of more than one record when not allowed',
              file=sys.stderr)
        print('Field name', field_name, file=sys.stderr)
        print('Field value', field_value, file=sys.stderr)
        sys.exit(1)

    # At this point, there is a set of pairs to assign
    if debug:
        print('Pair list:', str(pair_list), file=sys.stderr)

    # Loop over all the records obtained
    for user_data in user_records:
        # Get the dictionary and the user id from the record
        user_dict = json.loads(user_data.dictionary)
        user_id = user_data.user.id

        # Loop over the pairs given as parameters and update the content
        update_db = False
        for key, value in pair_list:
            # If the value is already there, skip it.
            if insert_only and user_dict.get(key) is not None:
                print('Key', key, 'already exists. Skipping.', file=sys.stderr)
                continue

            # Check the old value to see if it is different
            old_value = user_dict.get(key)

            # The two values (old, new) are identical, no need to modify
            if old_value == value:
                continue

            # At this point, the DB row needs to be updated
            update_db = True

            # Set the new value
            user_dict[key] = value

        if update_db:
            if debug:
                print('Updating user_id', user_id, file=sys.stderr)

            update_user_dictionary(user_id, context_name, user_dict)


def insert_context_members(context_name, debug=False):
    """
    Reads from standard input emails and sets them as members of the given
    context
    :param context_name: Name of the context to insert
    :param debug: Flag to print debug messages
    """

    # Check that the given context_name is correct
    elements = user_mgmt.models.Context.objects.filter(name=context_name)
    if len(elements) != 1:
        print('Incorrect context name', context_name, file=sys.stderr)
        sys.exit(1)

    for line in sys.stdin:
        line = line[:-1].strip()

        # If starts with comment or empty, skip
        if line == '' or line[0] == '#':
            continue

        if debug:
            print("Inserting user as context member", file=sys.stderr)

        insert_user_as_context_member(line, context_name)


def insert_context_members_from_csv_file(file_name,
                                         header_mark,
                                         lookup_column,
                                         context_name,
                                         debug=False):
    """
    Opens a CSV file, the information in one of the columns is used for user
    lookup and those users are inserted as members of the context with the
    given name.

    :param file_name: CSV file to read
    :param header_mark: String to detect the header line
    :param lookup_column: Name of column with infor for user lookup
    :param context_name: Name of the context to insert
    :param debug: Flag to print debug messages
    :return: Nothing is returned. The effect reflected in the DB
    """

    # Check that the given context_name is correct
    elements = user_mgmt.models.Context.objects.filter(name=context_name)
    if len(elements) != 1:
        print('Incorrect context name', context_name, file=sys.stderr)
        sys.exit(1)

    # Open the file for reading
    file_in = codecs.open(file_name, 'rU')
    dialect = csv.Sniffer().sniff(file_in.read(1024))
    file_in.seek(0)
    data_in = csv.reader(file_in, dialect)

    # Loop over the lines
    line_number = 0
    header_detected = False
    lookup_idx = -1
    for data in data_in:
        # Count the line number to flag anomalies
        line_number += 1

        # If mark has not been detected yet
        if not header_detected:
            if header_mark is not None and header_mark not in data:
                # Mark is given, but line does not match, skip it
                continue

            # At this point either the mark has been detected, or no mark has
            # been given, in which case, still take it as detected
            header_detected = True

            # If the column to look up the entry is not there, terminate
            if lookup_column in data:
                lookup_idx = data.index(lookup_column)
            else:
                print('Column', lookup_column, 'not found.', file=sys.stderr)
                print('Columns:', data, file=sys.stderr)
                sys.exit(1)

            if debug:
                print('Columns:', data, file=sys.stderr)
                print('Lookup idx:', lookup_idx, file=sys.stderr)

            # Proceed with the following lines
            continue

        # Safety check. If something went wrong when the CSV file was
        # exported, it is very likely that the string #REF! is present. If
        # so, notify and stop.
        if "#REF!" in data:
            print("Line", line_number, "contains incorrect data",
                  file=sys.stderr)
            sys.exit(1)

        # At this point we are processing a data line

        # If the number of fields does not match then number of columns Flag!
        if lookup_idx >= len(data):
            print("Mismatched line", line_number, "skipping", file=sys.stderr)
            continue

        # Insert context membership for the user identified in the column
        # with index lookup_idx

        # Insert the given user id in the context
        insert_user_as_context_member(data[lookup_idx], context_name, debug)

    return


def update_records_from_csv_file(context_name,
                                 file_name,
                                 select_columns,
                                 ignore_columns,
                                 header_mark,
                                 lookup_column,
                                 compare_column,
                                 column_prefix,
                                 debug=False,
                                 insert_only=False):
    """
    Function that receives a CSV file, a set of columns to ignore, a string
    to detect the header row, and a column to lookup user and inserts the
    information in the file in the database and creates the additional pairs
    (name, value) in the dictionary of users with the column prefix.

    :param context_name: string with the context name to consider
    :param file_name: File to open and read its content. Must be in CSV
    :param select_columns: List of columns to select
    :param ignore_columns: List of columns names to ignore (either one of
    this or the previous can be used
    :param header_mark: String to detect the header row in the file
    :param lookup_column: Column in the CSV to select the value for DB lookup
    :param compare_column: Column to use to for the user lookup in the DB
    :param column_prefix: Prefix to add to column names in dictionary
    :param debug: verbosity level
    :param insert_only: Allow insertion, not updates.
    :return:
    """

    # If the compare column is not given, it is assumed to be identical to
    # the lookup column
    if compare_column is None:
        compare_column = lookup_column

    # Open the file for reading
    file_in = codecs.open(file_name, 'rU')
    dialect = csv.Sniffer().sniff(file_in.read(10240))
    file_in.seek(0)
    data_in = csv.reader(file_in, dialect)

    # Loop over the lines
    line_number = 0
    header_detected = False
    file_column_names = []
    lookup_idx = -1
    num_columns = -1
    for data in data_in:
        # Count the line number to flag anomalies
        line_number += 1

        # If mark has not been detected yet
        if not header_detected:
            print('H', header_mark, data, file=sys.stderr)
            if header_mark is not None and header_mark not in data:
                # Mark is given, but line does not match, skip it
                continue

            # At this point either the mark has been detected, or no mark has
            # been given, in which case, still take it as detected
            header_detected = True

            # Get the column names
            file_column_names = data
            num_columns = len(file_column_names)

            # Keep the index of those to select or ignore
            if select_columns:
                select_idx = [file_column_names.index(x)
                              for x in file_column_names
                              if x in select_columns]
            else:
                select_idx = [file_column_names.index(x)
                              for x in file_column_names
                              if x not in ignore_columns]

            # If the column to look up the entry is not there, terminate
            if lookup_column in file_column_names:
                lookup_idx = file_column_names.index(lookup_column)
            else:
                print('Column', lookup_column, 'not found.', file=sys.stderr)
                print('Columns:', file_column_names, file=sys.stderr)
                sys.exit(1)

            # Filter the selected columns and stick the prefix
            file_column_names = [(column_prefix + file_column_names[x]) for x
                                 in select_idx]

            if not file_column_names:
                print('No columns selected. Review column names',
                      file=sys.stderr)
                sys.exit(1)

            if debug:
                print("Header detected in", file_name, file=sys.stderr)
                print('Columns:', file_column_names, file=sys.stderr)
                print('Select idx:', select_idx, file=sys.stderr)
                print('Lookup idx:', lookup_idx, file=sys.stderr)

            # Proceed with the following lines
            continue

        # Safety check. If something went wrong when the CSV file was
        # exported, it is very likely that the string #REF! is present. If
        # so, notify and stop.
        if '#REF!' in data:
            print('Line', line_number, 'contains incorrect data',
                  file=sys.stderr)
            sys.exit(1)

        # At this point we are processing a data line

        # If the number of fields does not match then number of columns Flag!
        if len(data) < num_columns:
            print('Mismatched line', line_number, 'skipping', file=sys.stderr)
            continue

        # Insert for the user identified by the lookup_column label and the
        # corresponding data all the fields in the column names selected.

        if debug:
            print("Updating data for", data[lookup_idx], file=sys.stderr)

        # Update the data
        perform_update_or_set_record(compare_column,
                                     data[lookup_idx],
                                     context_name,
                                     list(zip(file_column_names,
                                              [data[x] for x in select_idx])),
                                     debug,
                                     insert_only,
                                     True)


def dump_csv_records(user_data, output=sys.stdout):
    """
    :param user_data: List with pairs (user_record, dictionary)
    :param output: Stream where to dump the content of the records

    :return:
    """

    # Create the csv writer object
    data_out = csv.writer(output, delimiter=',', quotechar='"')

    # Get the column names and accumulate the dictionaries for each user
    dict_column_names = set([])
    user_dicts = []
    for user_record in user_data:
        user_dict = json.loads(user_record.dictionary)
        dict_column_names = dict_column_names.union(list(user_dict.keys()))
        user_dicts.append(user_dict)

    # Sort the resulting list of columns
    dict_column_names = sorted(dict_column_names)

    # Print the header line with the column names
    data_out.writerow(['user_id', ] + dict_column_names)

    # Print the rest of the content
    for user_record, record_dict in zip(user_data, user_dicts):
        # Three first columns
        data = [str(user_record.user.id)]

        # Rest of the columns
        data.extend([str(record_dict.get(x, '')) for x in dict_column_names])

        # Output the line
        data_out.writerow(data)

    # Done!
    return


def get_user_record(user_field, user_value, context_name):
    """
    Returns the PersonalRecord entry with user_field = user_value

    :param user_field: The user field to use to search (user_id or email)
    :param user_value: The value for that field
    :param context_name: String with the context name to filter
    :rtype: Element in PersonalRecord with that user_field=user_value

    if the field_name is user_id or email, and the record does not exist,
    it is created.
    """

    user_model = django.contrib.auth.get_user_model()
    if user_field == 'email':
        user_value = user_model.objects.get(email=user_value)
    elif user_field == 'user_id':
        user_value = user_model.objects.get(id=user_value)
    else:
        raise Exception('Field can only be email or user_id')

    return user_mgmt.user_operations.find_or_add_user_context_record(
              user_value,
              context_name
           )


def get_record_list(field_name, field_value, context_name):
    """
    Returns a user records as they are stored in the DB

    :param field_name: The field to look for in the dictionary of PersonalRecord
    :param field_value: The value for that field
    :param context_name: String with the context name to filter
    :rtype: list of PersonalRecord elements with the field_name equal to
    field_value
    """

    # Fetch all elements from the table with the given context (or all if
    # None is given)
    if context_name is None:
        user_records = user_mgmt.models.PersonalRecord.objects.all()
    else:
        user_records = user_mgmt.models.PersonalRecord.objects.filter(
            context__name=context_name)

    # If no key/value has been given to select, return all of them
    if (field_name is None or field_name == '') and \
            (field_value is None or field_value == ''):
        return user_records

    # Loop over all the user records to inspect dictionary and select
    user_data = []
    for user_record in user_records:

        # Get the dictionary
        dict_data = json.loads(user_record.dictionary)

        # See if this object is one of the selected ones
        if dict_data.get(field_name) is None or dict_data[field_name] != \
                field_value:
            # The dictionary does not contain the fieldname,
            # fieldvalue pair
            continue

        # Add the pair user_record, dictionary to the result
        user_data.append(user_record)

    # Return the list of found elements
    return user_data


def run(*script_args):
    """
    Execute various operations over the PersonalRecord table. The available
    commands are:

--------------------------------------------------------------------------------
    - remove_fields
      Options:
      -c name: context name
      -d : Turns on debugging (extra messages get printed)
      -k key Key to use for user lookup [OPTIONAL]
      -v value Value to use combined with the key to select user to modify
               [OPTIONAL]
      -r "field": Field to remove. It may appear several times.

      Example
        ./manage.py runscript -v 3 --traceback
            --settings=data2u.settings
            personal_record_ops
            --script-args="remove_fields
                           -c <CONTEXT NAME>
                           -k key
                           -v value
                           -d
                           -r 'Route code'"

--------------------------------------------------------------------------------
    - insert_users_from_csv_file
      Options:
      -c name: context name
      -d : Turns on debugging (extra messages get printed)
      -f : filename: CSV file
      -h : header_mark: string to detect the line that contains the column
           names. The first line containing this string is considered
           the first of the data section.
      -l column_to_select: string of a column to select in the upload

      Example
        ./manage.py runscript -v 3 --traceback
            --settings=data2u.settings
            personal_record_ops
            --script-args="insert_users_from_csv_file
                           -c <CONTEXT NAME>
                           -d
                           -f <FILE NAME>
                           -h 'Institutional email'
                           -l 'Institutional email'"

--------------------------------------------------------------------------------
    - insert_user_record
      Options:
      -c name: context name
      -d : Turns on debugging (extra messages get printed)
      -k key Key to use for user lookup [OPTIONAL]
      -v value key value to select the record [OPTIONAL]
      -p "('field', 'value')": A pair to assign to the selected user. It may
                               appear several times.

      Example
        ./manage.py runscript -v 3 --traceback
            --settings=data2u.settings
            personal_record_ops
            --script-args="insert_user_record
                           -c <CONTEXT NAME>
                           -d
                           -k key
                           -v key_value
                           -p \"('name', 'value')\""

--------------------------------------------------------------------------------
    - update_user_record
      Options:
      -c name: context name
      -d : Turns on debugging (extra messages get printed)
      -k key Key to use for user lookup [OPTIONAL]
      -v value key value to select the record [OPTIONAL]
      -p "('field', 'value')": A pair to assign to the selected user. It may
                                 appear several times.


      Example
        ./manage.py runscript -v 3 --traceback
            --settings=data2u.settings
            personal_record_ops
            --script-args="update_user_record
                           -c <CONTEXT NAME>
                           -d
                           -k key
                           -v key_value
                           -p \"('name', 'value')\""

--------------------------------------------------------------------------------
    - update_records_from_csv_file
      Options:
      -c name: context name
      -d : Turns on debugging (extra messages get printed)
      -f filename: CSV file
      -h header_mark: string to detect the line that contains the column
                      names. The first line containing this string is
                      considered the first of the data section.
      -l lookup_column: to lookup if the row in the CSV file
      -m compare_column: to lookup the row in the DB
      -i column_to_ignore: string of a column to ignore in the upload
      -s column_to_select: string of a column to select in the upload (either
                           this option or -i must be given
      -x prefix: prefix to add to the column names

      Example:

        ./manage.py runscript -v 3 --traceback \
              --settings=data2u.settings \
              personal_record_ops \
              --script-args="update_records_from_csv_file \
                -d \
                -c <CONTEXT NAME> \
                -f <FILE> \
                -h '<STRING TO DETECT HEADER>' \
                -l '<COLUMN NAME TO LOOK UP A USER' \
                -m 'email' \
                -s 'COLUMN NAME TO SELECT'"

--------------------------------------------------------------------------------
    - insert_context_members
      Read from stdin emails and makes the users members of the given context
      Options:
      -c name: context name
      -d : Turns on debugging (extra messages get printed)

      Example:

        cat <FILE> | ./manage.py runscript -v 3 --traceback \
              --settings=data2u.settings \
              personal_record_ops \
              --script-args="insert_context_members \
                -d \
                -c <CONTEXT NAME>"


--------------------------------------------------------------------------------
    - insert_context_members_from_csv_file
      Options:
      -c name: context name
      -d : Turns on debugging (extra messages get printed)
      -f filename: CSV file
      -h header_mark: string to detect the line that contain the column
                      names.
                      The first line containing this string is considered
                      the first of the data section.
      -l lookup_column: to lookup if the row in the CSV file
      -m compare_column: to lookup the row in the DB

      Example:
        ./manage.py runscript -v 3 --traceback
            --settings=data2u.settings
            personal_record_ops
            --script-args="insert_context_members_from_csv_file
                           -c <CONTEXT NAME>
                           -d
                           -f <FILE>
                           -h 'Institutional email'
                           -l 'Institutional email'"
                           -m email

--------------------------------------------------------------------------------
    - dump_to_csv_file
      Options:
      -c name: context name
      -d : Turns on debugging (extra messages get printed)
      -k: key value to lookup user
      -v: value to check the key to look up users

    """

    # If the script is run with no arguments, print the __doc__.
    argv = shlex.split(script_args[0])
    if len(argv) == 0:
        print(run.__doc__, file=sys.stderr)
        return

    # Default value for the options
    context_name = None    # -c string Context name
    debug = False          # -d No debug information printed
    file_name = None       # -f filename CSV File name to read
    header_mark = None     # -h name String to detect the header row in CSV
    ignore_columns = []    # -i name Columns to ignore in the CSV file
    key_code = ''          # -k key Key to identify the user
    lookup_column = None   # -l name of column in CSV to look up the user
    compare_column = None  # -m name of column in DB to look up the user
    pair_list = []         # -p "('field', 'val')" List of pairs to
    #  update/set
    field_list = []        # -r field Fields to remove
    select_columns = []    # -s name Columns to select in the CSV file
    value = ''             # -v value Value to consider the key to identify
    # the user
    column_prefix = ''     # -x prefix Prefix to add to column names

    # Parse the options
    try:
        opts, args = getopt.getopt(argv[1:], "c:df:h:i:k:l:m:p:r:s:v:x:")
    except getopt.GetoptError as e:
        print(e.msg, file=sys.stderr)
        print(run.__doc__, file=sys.stderr)
        sys.exit(2)

    # Process the arguments
    for optstr, optvalue in opts:
        if optstr == "-c":
            context_name = optvalue
        elif optstr == "-d":
            debug = True
        elif optstr == "-f":
            file_name = optvalue
        elif optstr == "-h":
            header_mark = optvalue
        elif optstr == "-i":
            ignore_columns.append(optvalue)
        elif optstr == "-k":
            key_code = optvalue
        elif optstr == "-l":
            lookup_column = optvalue
        elif optstr == "-m":
            compare_column = optvalue
        elif optstr == "-p":
            try:
                pair_list.append(eval(optvalue))
            except NameError:
                print('String', optvalue, end=' ', file=sys.stderr)
                print('is not a pair (key, value)', file=sys.stderr)
                return
        elif optstr == "-r":
            field_list.append(optvalue)
        elif optstr == "-s":
            select_columns.append(optvalue)
        elif optstr == "-v":
            value = optvalue
        elif optstr == "-x":
            column_prefix = optvalue

    # Context name is mandatory
    if context_name is None:
        print('Command needs -c context_name.', file=sys.stderr)
        print(run.__doc__, file=sys.stderr)
        sys.exit(1)

    # Check which command to execute
    if argv[0] == 'remove_fields':
        # There must be a key/value pair to select elements
        # if (key_code == '' and value != '') or \
        #         (key_code != '' and value == ''):
        #     print >> sys.stderr, 'No key/value provided to select users'
        #     print >> sys.stderr,  run.__doc__
        #     sys.exit(1)

        # If no pair list, then option s makes no sense
        if not field_list:
            print('Field list is empty.', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        if debug:
            print('Selector:', str(key_code), file=sys.stderr)

        remove_fields(key_code, value, field_list, context_name, debug)

    elif argv[0] == 'insert_users_from_csv_file':
        # File name needs to be given
        if file_name is None:
            print('Command needs a file name.', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        if not os.path.exists(file_name):
            print('File', file_name, 'not found.', file=sys.stderr)
            sys.exit(1)

        # Lookup column name is mandatory
        if lookup_column is None:
            print('Command needs a lookup column.', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        insert_users_from_csv_file(file_name, lookup_column, header_mark, debug)

    elif argv[0] == 'insert_user_record':
        # There must be a user_id to select the record
        if (key_code == '' and value != '') or \
                (key_code != '' and value == ''):
            print('No key/value provided to select users', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        # If pair list is empty, nothing to insert
        if not pair_list:
            print('No pairs given to insert. Use -p (name, value)',
                  file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        if debug:
            print('Selector:', str(key_code), file=sys.stderr)

        perform_update_or_set_record(key_code, value, context_name, pair_list,
                                     debug, False)

    elif argv[0] == 'update_user_record':
        # There must be a user_id to select the record
        if (key_code == '' and value != '') or \
                (key_code != '' and value == ''):
            print('No key/value provided to select users', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        if debug:
            print('Selector:', str(key_code), file=sys.stderr)

        perform_update_or_set_record(key_code,
                                     value,
                                     context_name,
                                     pair_list,
                                     debug,
                                     False)

    elif argv[0] == 'update_records_from_csv_file':
        # File name needs to be given
        if file_name is None:
            print('Command needs a file name.', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        if not os.path.exists(file_name):
            print('File', file_name, 'not found.', file=sys.stderr)
            sys.exit(1)

        # Lookup column name is mandatory
        if lookup_column is None:
            print('Command needs a lookup column.', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        if select_columns != [] and ignore_columns != []:
            print('Only one of -s or -i options allowed.', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        update_records_from_csv_file(context_name,
                                     file_name,
                                     select_columns,
                                     ignore_columns,
                                     header_mark,
                                     lookup_column,
                                     compare_column,
                                     column_prefix,
                                     debug)

    elif argv[0] == 'insert_context_members':

        insert_context_members(context_name, debug)

    elif argv[0] == 'insert_context_members_from_csv_file':

        # File name needs to be given
        if file_name is None:
            print('Command needs a file name.', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        if not os.path.exists(file_name):
            print('File', file_name, 'not found.', file=sys.stderr)
            sys.exit(1)

        # Lookup column name is mandatory
        if lookup_column is None:
            print('Command needs a lookup column.', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)

        insert_context_members_from_csv_file(file_name, header_mark,
                                             lookup_column, context_name,
                                             debug)

    elif argv[0] == 'dump_to_csv_file':
        if (key_code == '' and value != '') or \
                (key_code != '' and value == ''):
            print('No key/value provided to select users', file=sys.stderr)
            print(run.__doc__, file=sys.stderr)
            sys.exit(1)
        dump_csv_records(get_user_record(key_code, value, context_name))

    else:
        print('Command', argv[0], 'not found.', file=sys.stderr)
        print(run.__doc__, file=sys.stderr)
        return
