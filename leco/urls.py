#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from django.conf.urls import url

import leco.views

urlpatterns = [
    url(r'^$', leco.views.ActionView.as_view),
    url(r'^update_action/(?P<slug>\w+)/$', leco.views.ActionUpdate.as_view),
    url(r'^input$', leco.views.input_form_page, name='leco_input_page'),
    url(r'^get_attribute$', leco.views.get_attribute),
    url(r'^show_pyinfo$', leco.views.show_pyinfo),
    url(r'^post_entry$', leco.views.post_entry)
]
