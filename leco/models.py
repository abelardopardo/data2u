#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from django.db import models
from django.conf import settings


#
# Action
#
class Action(models.Model):
    
    name = models.CharField(max_length=256, primary_key=True, unique=True)

    # User that created this action
    creator = models.CharField("Creator", max_length=256)

    # Timestamp from which submissions are accepted
    accept_from = models.DateTimeField('From', null=True)

    # Timestamp until which submissions are accepted
    accept_until = models.DateTimeField('Until', null=True)

    def __str__(self):
        return self.name


#
# Action groups
#
class ActionGroup(models.Model):
                                         
    name = models.CharField("Group name", max_length=256)

    secret = models.CharField("Secret", max_length=256, unique=True)

    actions = models.ManyToManyField(Action)

    def __str__(self):
        return self.name


#
# User groups
# 
class UserGroup(models.Model):
                                         
    name = models.CharField("Group name", max_length=256)

    users = models.ManyToManyField(settings.AUTH_USER_MODEL)

    def __str__(self):
        return self.name


#
# Submission
#
class Submission(models.Model):
    
    # Timestamp
    received = models.DateTimeField(db_index=True)

    # Object
    context = models.TextField()    

    # Subject
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             db_index=True,
                             on_delete=models.CASCADE)

    # Verb
    action = models.ForeignKey(Action,
                               db_index=True,
                               on_delete=models.CASCADE)

    # Object
    payload = models.TextField()    

    http_host = models.CharField(verbose_name="Host", max_length=256,
                                 blank=True)
    http_referer = models.CharField(verbose_name="Referrer",
                                    max_length=256, blank=True)
    http_user_agent = models.CharField(max_length=256, blank=True)
    query_string = models.CharField(max_length=256, blank=True)
    remote_addr = models.CharField(max_length=256, blank=True)
    remote_host = models.CharField(max_length=256, blank=True)
    remote_user = models.CharField(max_length=256, blank=True)
    server_name = models.CharField(max_length=256, blank=True)
    server_port = models.CharField(max_length=256, blank=True)
    
    def __str__(self):
        return str(self.id)

    class Meta:
        ordering = ['id']


#
# (Logical file, physical file) pairs
# 
class UploadedFile(models.Model):
    
    logical_name = models.CharField("Logical name", max_length=256)
    physical_name = models.FileField("Physical name", upload_to='leco')
    submission = models.ForeignKey(Submission,
                                   on_delete=models.CASCADE)
