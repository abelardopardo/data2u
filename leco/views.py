#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import cgi
import logging
import os
import platform
import socket
import sys
import json

from django import forms
from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView

import leco.models
import user_mgmt.models
import user_mgmt.user_operations

# Suffix to add to the MEDIA_ROOT config variable to store files
upload_dir = 'uploads'

# Fixed name in the requests to detect the data
context_field_name = 'leco_context_field'

# Fixed name in the request to detect subject name
subject_field_name = 'leco_subject_field'

# Fixed name in the requests to detect action name
verb_field_name = 'leco_verb_field'

# Fixed name in the requests to detect the data
payload_field_name = 'leco_object_field'

# Creator of an action when not present in database
on_demand_creator = 'leco_on_demand'

# The logger to write messages
logger = logging.getLogger(__name__)


class InputForm(forms.Form):
    leco_subject_field = forms.CharField(label="Subject:", required=True,
                                         max_length=256)
    leco_verb_field = forms.CharField(label="Verb:", required=True)
    payload = forms.CharField(required=True)
    given_file = forms.FileField()
    given_file2 = forms.FileField()


def input_form_page(request):
    form = InputForm()
    return render(request,
                  'leco/input_form.html',
                  {'form': form,
                   'debug': {'m1': str(request.environ),
                             'm2': str(request.META)}
                   })


@csrf_exempt
def post_entry(request):
    """
    Function the receives an event to log. It is assumed to be authenticated,
    to be a POST request, to have some pre-defined fields, etc.

    :param request: Request received through the server
    :return:
    """
    global verb_field_name
    global subject_field_name
    global payload_field_name

    # print('Step 1')
    # Make sure the user is authenticated
    if not request.user.is_authenticated() and \
            not user_mgmt.user_operations.find_or_add(request,
                                                      subject_field_name):
        return render(request, 'leco/user_missing.html', {})

    # print('Step 2: Request method is %s', request.method)
    # If the request is a GET, return error page
    if request.method != 'POST':
        return render(request, 'leco/form_with_get.html', {})

    # print('Step 3: POST dict: ', request.POST)
    # If the post has no information attached, return immediately.
    non_empty_fields = [m for m, v in list(request.POST.items()) if v != '']
    if len(non_empty_fields) == 1 and len(request.FILES) == 0:
        return render(request, 'leco/empty_submission.html', {})

    # print('Step 4: Verb: ', request.POST.get(verb_field_name, None))
    # Make sure the verb field is present and get its value
    verb_field = request.POST.get(verb_field_name, None)
    if verb_field is None or verb_field == '' or verb_field == []:
        return render(request, 'leco/no_verb.html', {'verb': verb_field_name})

    # Insert the new action if it does not exist
    action = find_or_add_action(verb_field)

    # Process the correctly post submission
    # print('Step 5')
    new_submission = populate_submission(request.user, action,
                                         request.POST,
                                         request.META)

    # A few cases of events that have additional effects in the database,
    # mainly those that modify PersonalRecord

    # If the event is of type form-submit, update the information in the
    # personal record
    if action.name == 'form-submit':
        # Access the payload from the POST request
        payload = json.loads(request.POST.get(payload_field_name))
        # Obtain the two crucial fields
        form_id = payload.get('form_id', None)
        url_item = payload.get('url', None)
        # Keep processing only if the fields are not None
        if form_id is not None and url_item is not None:
            # Remove the fields to process the remaining fields
            payload.pop('form_id')
            payload.pop('url')

            # Create or update a personal record with the remaining items in
            # the payload
            user_mgmt.user_operations.create_or_update_user_context_record(
                request.user.email,
                request.POST.get(context_field_name, ''),
                [(form_id + '_' + x, y) for (x, y) in list(payload.items())]
            )
    elif action.name == 'xy-click':
        # If the event is of type xy-click, update the information in the
        # personal record as well
        # Access the payload from the POST request
        payload = json.loads(request.POST.get(payload_field_name))
        # Obtain the two crucial fields
        xy_id = payload.get('id', None)
        # Keep processing only if the id is not None
        if xy_id is not None:
            # Remove the fields to process the remaining fields
            payload.pop('id')
            payload.pop('url')
            payload.pop('title')

            # Create or update a personal record with the remaining items in
            # the payload
            user_mgmt.user_operations.create_or_update_user_context_record(
                request.user.email,
                request.POST.get(context_field_name, ''),
                [('xy_click_' + xy_id, payload)]
            )

    # Now process the file fields
    upload_prefix = os.path.join(settings.MEDIA_ROOT, upload_dir)
    for k, v in list(request.FILES.items()):
        file_name = \
            os.path.join(upload_prefix,
                         '_'.join([k, "{0:016x}".format(new_submission.pk)]))
        data_out = open(file_name, 'wb')
        for chunk in v:
            data_out.write(chunk)
        data_out.close()

        new_uploaded_file = \
            leco.models.UploadedFile(logical_name=k,
                                     physical_name=file_name,
                                     submission=new_submission)
        new_uploaded_file.save()

    return render(request,
                  'leco/post_info.html')


def get_attribute(request):
    """

    :param request: The HTTP request received through server
    :return:

    Function that given a key and a context corresponding to a previously
    submitted form, returns the content stored for that user in the
    personal record table or an empty string if the value has not been found, or
    any other anomaly has been found.
    """

    # Initial value for the response
    response = ''

    # Get the user information. If not authenticated, return empty string
    if not request.user.is_authenticated():
        # print 'USER IS NOT AUTH'
        return HttpResponse(json.dumps(response),
                            content_type='application/json')

    # Get the context. If not correct, return empty string
    context = request.GET.get('context', None)
    if context is None:
        # print 'NO CONTEXT FOUND'
        return HttpResponse(json.dumps(response),
                            content_type='application/json')

    # Obtain the personal record for the given user and context
    try:
        user_personal_record = \
            user_mgmt.models.PersonalRecord.objects.get(
                user__email=request.user, context=context)
    except user_mgmt.models.PersonalRecord.DoesNotExist:
        # print 'NO USER RECORD FOUND', request.user, context
        return HttpResponse(json.dumps(response),
                            content_type='application/json')

    # Try to parse the dictionary field and detect anomalies.
    try:
        user_dict = json.loads(user_personal_record.dictionary)
    except ValueError:
        # print 'DICT no JSON string', user_personal_record.dictionary
        return HttpResponse(json.dumps(response),
                            content_type='application/json')

    # If the user dictionary is NOT a dictionary, return
    if type(user_dict) is not dict:
        # print 'RECORD IS NOT DICT', request.user, context
        return HttpResponse(json.dumps(response),
                            content_type='application/json')

    # At this point, we found the personal record, it is a JSON string and it
    # contains a dictionary. We just need to get the given key, and return
    # the response.
    response = user_dict.get(request.GET.get('key', ''), '')
    return HttpResponse(json.dumps(response),
                        content_type="application/json")


class ActionUpdate(UpdateView):
    success_url = '/leco'
    model = leco.models.Action
    context_object_name = 'action'
    template_name = 'leco/update_action.html'
    slug_field = 'name'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UpdateView, self).dispatch(*args, **kwargs)


class ActionView(ListView):
    queryset = leco.models.Action.objects.order_by('name')
    context_object_name = 'current_actions'
    paginate_by = 15
    template_name = 'leco/index.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ListView, self).dispatch(*args, **kwargs)


def find_or_add_action(action_name):
    """
    Find an action with the given name. If it does not exist, create one,
    return the action object.
    """

    global on_demand_creator

    # Insert the new action if it does not exist
    action_qs = \
        leco.models.Action.objects.filter(name__exact=action_name)

    if action_qs.count() == 0:
        # Insert the action
        action = leco.models.Action(name=action_name,
                                    creator=on_demand_creator)
        action.save()
    else:
        action = action_qs[0]

    return action


def populate_submission(user, action, data, parameters):
    """
    Create a new submission object and populate its fields
    """

    global context_field_name
    global payload_field_name

    new_submission = leco.models.Submission(user=user)
    new_submission.received = timezone.now()
    new_submission.action = action
    new_submission.context = data.get(context_field_name, '')
    new_submission.payload = data.get(payload_field_name, '')

    new_submission.http_host = parameters.get('HTTP_HOST', '')
    new_submission.http_referer = parameters.get('HTTP_REFERER', '')
    new_submission.query_string = parameters.get('QUERY_STRING', '')
    new_submission.remote_addr = parameters.get('REMOTE_ADDR', '')
    new_submission.remote_host = parameters.get('REMOTE_HOST', '')
    new_submission.remote_user = parameters.get('REMOTE_USER', '')
    new_submission.server_name = parameters.get('SERVER_NAME', '')
    new_submission.server_port = parameters.get('SERVER_PORT', '')

    # Save the submission
    new_submission.save()

    return new_submission


def submission_accepted(request, submission_id):
    return render(request,
                  'leco/ack.html',
                  {'submission_id': submission_id})


@staff_member_required
def show_pyinfo(request):
    response = HttpResponse()
    output = pyinfo(request)
    response.write(output)
    return response


def pyinfo(request):
    output = '<!DOCTYPE html>\n'
    output += '<html>'
    output += '<head>'
    output += '<title>pyinfo()</title>'
    output += '<meta name="robots" '
    output += 'content="noindex,nofollow,noarchive,nosnippet">'
    output += styles()
    output += '</head>'
    output += '<body>'
    output += '<div class="center">'
    output += section_title()

    output += '<h2>System</h2>'
    output += section_system()

    output += '<h2>Python Internals</h2>'
    output += section_py_internals()

    output += '<h2>OS Internals</h2>'
    output += section_os_internals()

    output += '<h2>WSGI Environment</h2>'
    output += section_environ()

    output += '<h2>Request Environment</h2>'
    output += section_request(request)

    output += '<h2>Database support</h2>'
    output += section_database()

    output += '<h2>Compression and archiving</h2>'
    output += section_compression()

    # if sys.modules.has_key('ldap'):
    #     output += '<h2>LDAP support</h2>'
    #     output += section_ldap()

    if 'socket' in sys.modules:
        output += '<h2>Socket</h2>'
        output += section_socket()

    output += '<h2>Multimedia support</h2>'
    output += section_multimedia()

    output += '<h2>Copyright</h2>'
    output += section_copyright()

    output += '</div>'
    output += '</body>'
    output += '</html>'
    return output


def styles():
    return """
<style type="text/css">
  body{background-color:#fff;color:#000}
  body,td,th,h1,h2{font-family:sans-serif}
  pre{margin:0px;font-family:monospace}
  a:link{color:#009;text-decoration:none;background-color:#fff}
  a:hover{text-decoration:underline}
  table{border-collapse:collapse}
  .center{text-align:center}
  .center table{margin-left:auto;margin-right:auto;text-align:left}
  .center th{text-align:center !important}
  td,th{border:1px solid #999999;font-size:75%;vertical-align:baseline}
  h1{font-size:150%}
  h2{font-size:125%}
  .p{text-align:left}
  .e{width:30%;background-color:#ffffcc;font-weight:bold;color:#000}
  .h{background:url(\'http://python.org/images/header-bg2.png\')
     repeat-x;font-weight:bold;color:#000}'
  .v{background-color:#f2f2f2;color:#000}
  .vr{background-color:#cccccc;text-align:right;color:#000}
  img{float:right;border:0px;}
  hr{width:600px;background-color:#ccc;border:0px;height:1px;color:#000}
  </style>"""


def table(html):
    return '<table border="0" cellpadding="3" width="600">%s</table><br>' % html


def makecells(data):
    html = ''
    while data:
        html += '<tr><td class="e">%s</td><td class="v">%s</td></tr>' % (
            data.pop(0), data.pop(0))
    return table(html)


def imported(mod):
    if mod in sys.modules:
        return 'enabled'
    return 'disabled'


def section_title():
    html = """
<tr class="h"><td>
<a href="http://python.org/"><img border="0"
src="http://python.org/images/python-logo.gif"></a>
<h1 class="p">Python %s</h1></td></tr>""" % platform.python_version()
    return table(html)


def section_system():
    data = []
    if hasattr(sys, 'subversion'):
        data += 'Python Subversion', ', '.join(sys.subversion)
    if platform.dist()[0] != '' and platform.dist()[1] != '':
        data += 'OS Version', '%s %s (%s %s)' % (
            platform.system(), platform.release(),
            platform.dist()[0].capitalize(),
            platform.dist()[1])
    else:
        data += 'OS Version', '%s %s' % (platform.system(), platform.release())
    if hasattr(sys, 'executable'):
        data += 'Executable', sys.executable
    data += 'Build Date', platform.python_build()[1]
    data += 'Compiler', platform.python_compiler()
    if hasattr(sys, 'api_version'):
        data += 'Python API', sys.api_version
    return makecells(data)


def section_py_internals():
    data = []
    if hasattr(sys, 'builtin_module_names'):
        data += 'Built-in Modules', ', '.join(sys.builtin_module_names)
    data += 'Byte Order', sys.byteorder + ' endian'
    if hasattr(sys, 'getcheckinterval'):
        data += 'Check Interval', sys.getcheckinterval()
    if hasattr(sys, 'getfilesystemencoding'):
        data += 'File System Encoding', sys.getfilesystemencoding()
    data += 'Maximum Integer Size', str(sys.maxsize) + ' (%s)' % str(
        hex(sys.maxsize)).upper().replace("X", "x")
    if hasattr(sys, 'getrecursionlimit'):
        data += 'Maximum Recursion Depth', sys.getrecursionlimit()
    if hasattr(sys, 'tracebacklimit'):
        data += 'Maximum Traceback Limit', sys.tracebacklimit
    else:
        data += 'Maximum Traceback Limit', '1000'
    data += 'Maximum Unicode Code Point', sys.maxunicode
    return makecells(data)


def section_os_internals():
    data = []
    if hasattr(os, 'getcwd'):
        data += 'Current Working Directory', os.getcwd()
    if hasattr(os, 'getegid'):
        data += 'Effective Group ID', os.getegid()
    if hasattr(os, 'geteuid'):
        data += 'Effective User ID', os.geteuid()
    if hasattr(os, 'getgid'):
        data += 'Group ID', os.getgid()
    if hasattr(os, 'getgroups'):
        data += 'Group Membership', ', '.join(map(str, os.getgroups()))
    if hasattr(os, 'linesep'):
        data += 'Line Seperator', repr(os.linesep)[1:-1]
    if hasattr(os, 'getloadavg'):
        data += 'Load Average', \
                ', '.join(map(str, [round(x, 2) for x in os.getloadavg()]))
    if hasattr(os, 'pathsep'):
        data += 'Path Seperator', os.pathsep
    if hasattr(os, 'getpid') and hasattr(os, 'getppid'):
        data += 'Process ID', \
                ('%s (parent: %s)' % (os.getpid(), os.getppid()))
    if hasattr(os, 'getuid'):
        data += 'User ID', os.getuid()
    return makecells(data)


def section_request(request):
    envvars = list(request.environ.keys())
    envvars.sort()
    data = []
    for envvar in envvars:
        data += envvar, cgi.escape(str(request.environ[envvar]))
    return makecells(data)


def section_environ():
    envvars = list(os.environ.keys())
    envvars.sort()
    data = []
    for envvar in envvars:
        data += envvar, cgi.escape(str(os.environ[envvar]))
    return makecells(data)


def section_database():
    data = []
    data += 'DB2/Informix (ibm_db)', imported('ibm_db')
    data += 'MSSQL (adodbapi)', imported('adodbapi')
    data += 'MySQL (MySQL-Python)', imported('MySQLdb')
    data += 'ODBC (mxODBC)', imported('mxODBC')
    data += 'Oracle (cx_Oracle)', imported('cx_Oracle')
    data += 'PostgreSQL (PyGreSQL)', imported('pgdb')
    data += 'Python Data Objects (PyDO)', imported('PyDO')
    data += 'SAP DB (sapdbapi)', imported('sapdbapi')
    data += 'SQLite3', imported('sqlite3')
    return makecells(data)


def section_compression():
    data = []
    data += 'Bzip2 Support', imported('bz2')
    data += 'Gzip Support', imported('gzip')
    data += 'Tar Support', imported('tarfile')
    data += 'Zip Support', imported('zipfile')
    data += 'Zlib Support', imported('zlib')
    return makecells(data)


# def section_ldap():
#     data = []
#     # data += 'Python-LDAP Version' % urls['Python-LDAP'], ldap.__version__
#     data += 'API Version', ldap.API_VERSION
#     data += 'Default Protocol Version', ldap.VERSION
#     data += 'Minimum Protocol Version', ldap.VERSION_MIN
#     data += 'Maximum Protocol Version', ldap.VERSION_MAX
#     data += 'SASL Support (Cyrus-SASL)', ldap.SASL_AVAIL
#     data += 'TLS Support (OpenSSL)', ldap.TLS_AVAIL
#     data += 'Vendor Version', ldap.VENDOR_VERSION
#     return makecells(data)


def section_socket():
    data = []
    data += 'Hostname', socket.gethostname()
    data += 'Hostname (fully qualified)', \
            socket.gethostbyaddr(socket.gethostname())[0]
    try:
        data += 'IP Address', socket.gethostbyname(socket.gethostname())
    except Exception:
        pass
    data += 'IPv6 Support', getattr(socket, 'has_ipv6', False)
    data += 'SSL Support', hasattr(socket, 'ssl')
    return makecells(data)


def section_multimedia():
    data = []
    data += 'AIFF Support', imported('aifc')
    data += 'Color System Conversion Support', imported('colorsys')
    data += 'curses Support', imported('curses')
    data += 'IFF Chunk Support', imported('chunk')
    data += 'Image Header Support', imported('imghdr')
    data += 'OSS Audio Device Support', imported('ossaudiodev')
    data += 'Raw Audio Support', imported('audioop')
    data += 'Raw Image Support', imported('imageop')
    data += 'SGI RGB Support', imported('rgbimg')
    data += 'Sound Header Support', imported('sndhdr')
    data += 'Sun Audio Device Support', imported('sunaudiodev')
    data += 'Sun AU Support', imported('sunau')
    data += 'Wave Support', imported('wave')
    return makecells(data)


def section_copyright():
    html = """
<tr class="v"><td>%s</td></tr>""" \
           % sys.copyright.replace('\n\n', '<br>').replace(
            '\r\n', '<br />').replace('(c)', '&copy;')
    return table(html)


optional_modules_list = [
    'Cookie',
    'zlib', 'gzip', 'bz2', 'zipfile', 'tarfile',
    'ldap',
    'socket',
    'audioop', 'curses', 'imageop', 'aifc', 'sunau', 'wave', 'chunk',
    'colorsys', 'rgbimg', 'imghdr', 'sndhdr', 'ossaudiodev', 'sunaudiodev',
    'adodbapi', 'cx_Oracle', 'ibm_db', 'mxODBC', 'MySQLdb', 'pgdb', 'PyDO',
    'sapdbapi', 'sqlite3'
]

for i in optional_modules_list:
    # noinspection PyBroadException
    try:
        module = __import__(i)
        sys.modules[i] = module
        globals()[i] = module
    except Exception:
        pass
