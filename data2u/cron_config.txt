* Crontab command:

0,5,10,15,20,25,30,35,40,45,50,55 * * * * (export DJANGO_SETTINGS_MODULE=data2u.settings; /usr/bin/flock -n /tmp/data2u_process.lockfile -c "python /home/ubuntu/django/accumulate/process.py >$HOME/Logs/process_`date +%y%m%d%H%M%S`.log 2>&1")

* Flock examples:

flock -n /tmp/path.to.lockfile -c command with args

In cron: 
  15 * * * * /usr/bin/flock -n /tmp/fcj.lockfile /usr/local/bin/frequent_cron_job --minutely
