#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

from django.shortcuts import render

import data2u


def about(request):
    """
    :param request: HTTP request received

    Page to show the version number, author and title.
    """
    
    # Get the version number
    render_dict = {'version_id': data2u.__version__,
                   'title': data2u.__tool_title__}

    # Once user and week has been selected, get the data and render
    return render(request, "about.html", render_dict)

# Execution as script
if __name__ == "__main__":
    print(data2u.__tool_title__)
    print("Version:", data2u.__version__)
