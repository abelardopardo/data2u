#!/bin/bash
#
# Script that renames the version to the new date
#
sed -i -e "s/^__version__ = '.*'$/__version__ = '"`date +%y%m%d`"'/g" \
    __init__.py
