#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from django.conf import settings
from django.conf.urls import include, url

import data2u.views

from django.contrib import admin
admin.autodiscover()

# Change the URL prefix for development or production
if settings.LOCAL_DEV:
    url_prefix = 'data2u/'
else:
    url_prefix = ''

urlpatterns = [
    # '',
    url(r'^' + url_prefix + 'about', data2u.views.about),
    url(r'^' + url_prefix + 'leco/', include('leco.urls')),
    url(r'^' + url_prefix + 'exco/', include('exco.urls')),
    url(r'^' + url_prefix + 'dboard/', include('dboard.urls')),
    url(r'^' + url_prefix + 'dout/', include('dout.urls')),
    url(r'^' + url_prefix + 'admin/', admin.site.urls),
    ]

if settings.LOCAL_DEV:
    import debug_toolbar
    urlpatterns.append(url(r'^__debug__/', include(debug_toolbar.urls)))
