#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function
from future.moves.urllib.parse import urlparse, urlencode, urlunparse, parse_qs

import sys
import string
# import urllib.parse
# import urllib.request, urllib.parse, urllib.error
import re
import json
import os
import logging
import random
from datetime import datetime

from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.conf import settings
from django import forms
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from operator import add
from django.template.loader import get_template, TemplateDoesNotExist

import exco.models
import leco.views
import user_mgmt.user_operations

# The logger to write messages
logger = logging.getLogger(__name__)

exco_instantiate_url = os.path.join('exco', 'instantiate')


class InputSequence(forms.Form):
    name = forms.CharField(label="Name", required=True, max_length=256)

    stage_urls = forms.CharField(label="Space separated stage urls",
                                 required=True,
                                 widget=forms.Textarea)

    stage_max_scores = forms.CharField(label="Comma separated max scores",
                                       required=True,
                                       widget=forms.Textarea)

    number_of_choices = forms.IntegerField(label="Number of choices")


def input_sequence_form(request):
    form = InputSequence()
    return render(request, 'exco/input_sequence.html', {'form': form})


def input_sequence_process(request):
    """
    :param request: Object with the HTML request
    :return:
    """

    # None of the lists is empty
    if request.POST['stage_urls'].strip() == '' or \
       request.POST['stage_max_scores'].strip() == '':
        status = "Neither the list of URLs or scores can be empty."
        return render(request, 'exco/show_sequence.html', {'status': status})

    # Check the list of URLs is non empty
    url_list = request.POST['stage_urls'].split(' ')
    url_list = list(map(str.strip, url_list))

    # Check that the list of scores is made out of integers
    score_list = request.POST['stage_max_scores'].split(',')
    score_list = list(map(str.strip, score_list))
    try:
        score_list = list(map(int, score_list))
    except ValueError:
        status = "The scores must be valid integers."
        return render(request, 'exco/show_sequence.html',
                      {'status': status})

    # List of urls and scores have identical length
    if len(score_list) != len(url_list):
        status = "The list of URLs and scores must have the same length."
        return render(request, 'exco/show_sequence.html', {'status': status})

    # Get the number of choices for the sequence
    num_choices = request.POST.get('number_of_choices', None)
    if num_choices is None:
        num_choices = 4
    else:
        try:
            num_choices = int(num_choices)
        except ValueError:
            status = "The number of choices must be a positive integer."
            return render(request,
                          'exco/show_sequence.html',
                          {'status': status})
        if num_choices < 1:
            status = "The number of choices must be a positive integer."
            return render(request,
                          'exco/show_sequence.html',
                          {'status': status})

    p_sequence = exco.models.ProblemSequence(name=request.POST['name'],
                                             stage_urls=url_list,
                                             stage_max_scores=score_list,
                                             number_of_choices=num_choices)
    p_sequence.save()
    status = "Sequence inserted correctly."

    return render(request, 'exco/show_sequence.html', {'status': status})


def sequence_step(request):
    """
    :param request: Object with the HTML request
    :return:

    Function that receives as parameter a sequence identifier and performs the
    following steps:

    1) If there is no sequence identifer, looks up the user and pulls out all
    the sequences activated.
        1.a If there are multiple sequences, a page to choose one is created
        and the links point back to this URL, each of them with the
        appropriate request parameter.
        1.b If there is one single sequence, it goes directly to the next
        problem on that sequence

    2) Pull out the information for the user/sequence from the database.

    3) Get the URLs and calculate the number of times each problem has been
    solved (either correctly or incorrectly). Select those with the minimum
    number of attempts.

    4) Select one of them and invoke the problem in an iFrame. The selection
    is done randomly!

    5) Offer options, a button to grade, the score and the button to go to the
    next problem.

    The page is rendered with the following dictionary:

    title: Overall page title
    iframe_url : URL of the problem to render
    titlesuffix: Title suffix on the page
    number_answers: Number of multiple choice answers allowed
    """

    # Make sure the user is authenticated, if not, get the user name from the
    # "email field".
    if not request.user.is_authenticated() and \
            not user_mgmt.user_operations.find_or_add(request):
        return render(request, 'leco/user_missing.html', {})

    # Get the sequence_id from the request (GET parameter)
    sequence_id = request.GET.get('sequence_id')

    # Get the user name and see if there are any sequences assigned.
    username = request.user.email
    if sequence_id is None:
        sequences = exco.models.SequenceStatus.objects.filter(
            user__email=username).filter(
            current=True)
    else:
        sequences = exco.models.SequenceStatus.objects.filter(
            user__email=username).filter(
            problem_sequence=sequence_id).filter(
            current=True)

    # If the result is empty, flag it as error
    if len(sequences) == 0:
        message = "You have no problems assigned to you."
        return render(
            request,
            'exco/no_sequences_assigned.html',
            {'message': message, 'user_id': username})

    # If the result returns more than one sequence, go to a page to choose.
    if len(sequences) > 1:
        # choose_sequence(request, sequences)
        choices = [("sequence_step?sequence_id=" + x.problem_sequence.name,
                    x.problem_sequence.description)
                   for x in sequences]
        return render(
            request,
            'exco/exco_select_sequence.html',
            {'choices': choices, 'user_id': username})

    # At this point, there is only one sequence selected
    sequence_status = sequences[0]
    sequence_id = sequence_status.problem_sequence

    # Fetch the problem_sequence_info
    sequence_info = \
        exco.models.ProblemSequence.objects.filter(name=sequence_id)
    if len(sequence_info) == 0:
        message = 'The sequence "%s" does not exist. Report this error.' \
                  % sequence_id
        return render(request, 'severe_error.html', {'message': message})

    # This variable contains the information about the generic sequence
    sequence_info = sequence_info[0]

    # Default dictionary to render the page
    render_dict = {
        'title': 'EXCO: Exercise Collections',
        'titlesuffix': '',
        'render_problem': True,
        'render_choices': True,
        'render_end_msg': False,
        'sequence_id': sequence_id,
        'reset_button': False,
        'url_home': sequence_info.url_home,
        'url_home_text': sequence_info.url_home_text,
        'choices': string.ascii_uppercase[0:sequence_info.number_of_choices],
        'score': "%.2f" % sequence_status.score
    }

    # If the sequence has reached maximum score, render special page.
    if sequence_status.score >= 100.0:
        render_dict['render_problem'] = False
        render_dict['reset_button'] = True
        render_dict['render_choices'] = False
        render_dict['render_end_msg'] = True

        # Send event also to LECO database (if enabled)
        if settings.EVENT_RECORD_URL != '':
            # Action to register the event
            action = leco.views.find_or_add_action('exco-done')

            # Get the name of the context to use when sending the event to LECO
            context_id = sequence_info.context_id

            # Create the data payload
            data = {leco.views.context_field_name: context_id,
                    leco.views.subject_field_name: username,
                    leco.views.verb_field_name: 'exco-done',
                    leco.views.payload_field_name:
                        json.dumps({'sequence': sequence_id.name})}

            # Push the event and store it in the DB
            _ = leco.views.populate_submission(request.user,
                                               action,
                                               data,
                                               request.META)

        return render(request, 'exco/sequence_step.html', render_dict)

    # Make sure the data is consistent with format
    sequence_urls = sequence_info.stage_urls.strip().split(' ')
    sequence_length = len(sequence_urls)

    # Get the solutions for the problems from the given URLs, store the data
    # in a database field so that the computation is only done once
    if sequence_info.solutions is None or sequence_info.solutions == '':
        # Parse the URL, obtain the query string and translate it into a
        # dictionary access the 'excosol' item and concatenate all of them.
        l = [parse_qs(urlparse(url).query).get('excosol',
                                                                 ['-1'])[0]
             for url in sequence_urls]

        try:
            # Make sure all of them are within the right limits
            if [int(x) for x in l if int(x) >= sequence_info.number_of_choices]:
                message = 'The sequence of URLs has incorrect ' + \
                          'solution information. Please report this issue.'
                return render(request,
                              'severe_error.html',
                              {'message': message})
        except ValueError:
            message = 'Incorrect solution info in sequence.' + \
                      'Please report this issue.'
            return render(request,
                          'severe_error.html',
                          {'message': message})

        # Turn the list into a string which is what is stored in the db
        sequence_info.solutions = ','.join(l)

        # Change the URLs so that they do not contain the solution at the end
        sequence_urls = [re.sub(r'excosol=[a-zA-Z0-9]*&*', r'', x)
                         for x in sequence_urls]
        sequence_info.stage_urls = ' '.join([re.sub(r'\?$', r'', x)
                                             for x in sequence_urls])

        # Update the info immediately.
        sequence_info.save()

    # Translate the comma separated lists into lists of integers. The
    # exception is to detect malformed integers.
    try:
        if sequence_status.correct == '':
            sequence_correct = [0] * sequence_length
            sequence_status.correct = ','.join(str(x)
                                               for x in sequence_correct)
        else:
            sequence_correct = [int(y)
                                for y in sequence_status.correct.split(',')]

        if sequence_status.incorrect == '':
            sequence_incorrect = [0] * sequence_length
            sequence_status.incorrect = ','.join(str(x) for x in
                                                 sequence_incorrect)
        else:
            sequence_incorrect = [int(y)
                                  for y in sequence_status.incorrect.split(',')]

    except ValueError:
        message = 'Data corrupted in sequence %s. Please notify coordinator' \
                  % sequence_info.name
        return render(request,
                      'severe_error.html',
                      {'message': message})

    # Calculate the list containing the number of attempts (adding correct and
    # incorrect answer to each problem
    sequence_attempts = list(map(add, sequence_correct, sequence_incorrect))
    sequence_min_attempt = min(sequence_attempts)

    # Set the start time if needed, and the last_access and update the record
    sequence_status.last_access = timezone.now()
    if sequence_status.start_time is None:
        # First time the sequence is started
        sequence_status.start_time = sequence_status.last_access

    # Obtain the list of exercises in the sequence to process. Each element
    # contains:
    # 1) The url of the problem
    # 2) The index in the sequence
    # 3) The number of attempts (for the user)
    # 4) The number of times that has been answered correctly
    sequence = list(zip(sequence_urls,  # Element[0]
                    list(range(0, sequence_length)),  # Element[1]
                    sequence_attempts,  # Element[2]
                    sequence_correct))  # Element[3]

    # Select an exercise and dispatch the page

    # First select a problem that has not been answered correctly yet!
    not_correct_yet = [x for x in sequence if x[3] == 0]

    # If there are problems that have not been answered correctly yet, select
    # one of them!
    if len(not_correct_yet) != 0:
        sequence = not_correct_yet
    else:
        # Trim first the sequence to only those that have not reached the
        # maximum score
        sequence = [x for x in sequence if x[2] == sequence_min_attempt]

    # If there is more than one problem left and there has been a previous
    # attempt, remove that element to avoid repetition
    if len(sequence) > 1 and sequence_status.last_problem != -1:
        sequence = [x for x in sequence
                    if x[1] != sequence_status.last_problem]

    # Select one pending exercise randomly and obtain its index inside the
    # sequence
    problem_unit = sequence[random.randint(0, len(sequence) - 1)]
    problem_index = sequence_urls.index(problem_unit[0])

    # Remove the suffix from the URL containing the solution!
    problem_unit, rseed = process_problem_url(problem_unit[0], 'excosol')

    # If there was a seed added to the problem URL, needs to be passed to
    # the template.
    if rseed is not None:
        render_dict['rseed'] = rseed

    # Update the problem index
    sequence_status.last_problem = problem_index
    sequence_status.save()

    # Send event also to LECO database (if enabled)
    if settings.EVENT_RECORD_URL != '':
        # Action to register the event
        action = leco.views.find_or_add_action('exco-view')

        # Exercise name (URL of the problem)
        exercise = sequence_info.stage_urls.split(' ')[problem_index]

        # Get the name of the context to use when sending the event to LECO
        context_id = sequence_info.context_id

        # Create the data payload
        data = {leco.views.context_field_name: context_id,
                leco.views.subject_field_name: username,
                leco.views.verb_field_name: 'exco-view',
                leco.views.payload_field_name:
                    json.dumps({'exercise': exercise,
                                'sequence': sequence_id.name,
                                'score': sequence_status.score,
                                'seed': rseed})}

        # Push the event and store it in the DB
        _ = leco.views.populate_submission(request.user,
                                           action,
                                           data,
                                           request.META)

    # Set the iframe_url parameter to the corresponsing URL
    render_dict['iframe_url'] = problem_unit
    render_dict['user_id'] = username
    render_dict['problem_index'] = problem_index
    return render(request, 'exco/sequence_step.html', render_dict)


@csrf_exempt
def update_attempt(request):
    """
    :param request: Object containing the HTML request

    Function to process a POST request with the following fields:
    - sequence_id: identifying the sequence where the problem is
    - problem_index: URL of the problem invoked.
    - outcome: either correct or incorrect.

    The post events received by this function are assumed to be produced inside
    a javascript, and therefore, the response is always an empty string.
    :return:
    """

    # First check if the POST has the right elements. if not IGNORE!
    sequence_id = request.POST.get('sequence_id', None)
    # Get the index of the problem in the sequence as an integer
    problem_index = int(request.POST.get('problem_index', None))
    # Get the answer typed by the user
    given_answer = int(request.POST.get('selected', None))
    # Get the seed to identify the problem (if it exists)
    rseed = int(request.POST.get('rseed', None))
    username = request.user.email

    if sequence_id is None or problem_index is None or given_answer is None:
        # FIX: Drop a line in a file to detect anomalies.
        return HttpResponse('')

    # Fetch the information about the sequence status
    sequence = exco.models.SequenceStatus.objects.filter(
            user__email=username).filter(
            problem_sequence=sequence_id).filter(
            current=True)

    # If the result is empty or larger than one, it is an error, ignore
    if len(sequence) == 0 or len(sequence) > 1:
        # FIX: Drop a line in a file to detect anomalies.
        return HttpResponse('')

    # Get the sequence status
    sequence_status = sequence[0]

    # Get sequence info to calculate the score
    sequence_info = \
        exco.models.ProblemSequence.objects.filter(name=sequence_id)

    # If there is no sequence info, report the error
    if len(sequence_info) != 1:
        # FIX: Drop a line in a file to detect anomalies.
        message = 'The sequence "%s" disappeared!. Report this error.' \
                  % sequence_id
        return render(request, 'severe_error.html', {'message': message})
    sequence_info = sequence_info[0]

    # Set the variable outcome to correct/incorrect after comparing the received
    # answer with the correct answer obtained from the sequence.
    outcome = 'incorrect'

    # Get the solution from the sequence
    correct_answer = int(sequence_info.solutions.split(',')[problem_index])

    # If the correct answer is -1, it means the correct answer needs to be
    # computed by the corresponding class
    if correct_answer == -1:
        # Get the module name and the exercise from the URL
        parsed = urlparse(sequence_info.stage_urls.split(' ')[problem_index])
        module_name, exercise = os.path.split(parsed.path)
        exercise, _ = os.path.splitext(exercise)
        _, module_name = os.path.split(module_name)

        # Get the template dir. Asuming it is the first one in the DIRS config
        template_dir = settings.TEMPLATES[0]['DIRS'][0]

        # Obtain the render dictionary from the module/exercise giving rseed.
        try:
            # See if the folder has a __init__.py file
            render_dict = \
                load_exercise_module(os.path.join(template_dir,
                                                  exco_instantiate_url,
                                                  module_name),
                                     exercise,
                                     rseed)
        except Exception as e:
            return render(request, 'exco/wrong_exercise.html',
                          {'error_msg': str(e)})

        # Get the solution from the render dictionary dictionary
        correct_answer = render_dict['@@exercise_solution@@']
    else:
        correct_answer = int(sequence_info.solutions.split(',')[problem_index])

    if correct_answer == given_answer:
        outcome = 'correct'

    # Increase either the correct or incorrect counter, and update document
    if outcome == 'correct':
        l = [int(x) for x in sequence_status.correct.split(',')]
        l[problem_index] += 1

        # Re-calculate the score as the proportion of correct answers with
        # respect to the maximum score.
        score = sum(l) * 100.0 / sequence_info.max_score
        if score > 100.0:
            score = 100.0

        # Update score and the correct count
        sequence_status.score = score
        sequence_status.correct = ','.join(str(x) for x in l)
    else:
        l = sequence_status.incorrect.split(',')
        l[problem_index] = str(int(l[problem_index]) + 1)
        sequence_status.incorrect = ','.join(x for x in l)

    # Update the finish_time if needed
    if sequence_status.finish_time is None and sequence_status.score >= 100.0:
        sequence_status.finish_time = timezone.now()

    # Send event also to LECO database (if enabled)
    if settings.EVENT_RECORD_URL != '':
        # Action to register the event
        action = leco.views.find_or_add_action('exco-answer')

        # Exercise name (URL of the problem)
        exercise = sequence_info.stage_urls.split(' ')[problem_index]

        # Get the name of the context to use when sending the event to LECO
        context_id = sequence_info.context_id

        # Create the data payload
        data = {leco.views.context_field_name: context_id,
                leco.views.subject_field_name: username,
                leco.views.verb_field_name: 'exco-answer',
                leco.views.payload_field_name:
                    json.dumps({'exercise': exercise,
                                'outcome': outcome,
                                'sequence': sequence_id,
                                'score': sequence_status.score,
                                'seed': rseed})}

        # Push the event and store it in the DB
        _ = leco.views.populate_submission(request.user,
                                           action,
                                           data,
                                           request.META)

    # Update the data
    sequence_status.save()

    # Send as response data the json object with the outcome
    response_data = {'result': outcome}
    return HttpResponse(json.dumps(response_data),
                        content_type="application/json")


def reset_sequence(request):
    """
    :param request: Request that resets a set of problems. It does that
    creating a duplicate of the status document, as we want to keep a
    history of the multiple sequences attempted.
    """

    # Get the sequenceId
    sequence_id = request.GET.get('sequence_id', None)
    if sequence_id is None:
        message = 'The request to reset a sequence is incorrectly formed. '
        message += 'Report this message to the responsible person.'
        return render(request, 'severe_error.html', {'message': message})

    # Get the sequence status to reset
    username = request.user.email
    sequence = exco.models.SequenceStatus.objects.filter(
        user__email=username,
        problem_sequence=sequence_id,
        current=True)

    # If the result is empty or larger than one, it is an error. Report.
    if len(sequence) != 1:
        message = \
            'The request to reset a sequence did not find any sequence. '
        message += 'Please report this issue to the responsible person.'
        return render(request, 'severe_error.html', {'message': message})
    # Manipulate a single sequence document
    sequence = sequence[0]

    # First set the current to false and save the new value
    sequence.current = False
    sequence.save()

    # Create the duplicate with current = True
    new_sequence = exco.models.SequenceStatus(
            user=sequence.user,
            problem_sequence=sequence.problem_sequence,
            current=True)
    new_sequence.save()

    # Send event also to LECO database (if enabled)
    if settings.EVENT_RECORD_URL != '':
        # Action to register the event
        action = leco.views.find_or_add_action('exco-reset')

        # Get the name of the context to use when sending the event to LECO
        context_id = sequence.problem_sequence.context_id

        # Create the data payload
        data = {leco.views.context_field_name: context_id,
                leco.views.subject_field_name: username,
                leco.views.verb_field_name: 'exco-reset',
                leco.views.payload_field_name:
                    json.dumps({'sequence': sequence_id})}

        # Push the event and store it in the DB
        _ = leco.views.populate_submission(request.user,
                                           action,
                                           data,
                                           request.META)

    # And now, back to square one!
    return HttpResponseRedirect('sequence_step?sequence_id=%s' % sequence_id)


def instantiate_exercise(request, module_name, exercise):
    # Get the seed from the request. No seed, it is an anomaly.
    rseed = request.GET.get('rseed')

    # If  there is no seed, we cannot do anything!
    if rseed is None:
        return render(request, 'exco/wrong_exercise.html',
                      {'error_msg': 'Seed not found.'})

    # Translate to integer
    rseed = int(rseed)

    # Get the name of the template from the module name and exercise
    template = os.path.join(exco_instantiate_url,
                            module_name,
                            exercise + '.html')

    # Get the template dir. Asuming it is the first one in the DIRS config
    template_dir = settings.TEMPLATES[0]['DIRS'][0]

    # Obtain the render dictionary from the module/exercise giving rseed.
    render_dict = load_exercise_module(os.path.join(template_dir,
                                                    exco_instantiate_url,
                                                    module_name),
                                       exercise,
                                       rseed)
    if render_dict is None:
        render_dict = {}

    # Try to load the template
    try:
        get_template(template)
    except TemplateDoesNotExist as e:
        # Send exception to admins!
        alogger = logging.getLogger('mail_admins')
        alogger.error('Incorrect exercise:' + module_name + ', ' + exercise)
        return render(request, 'exco/wrong_exercise.html',
                      {'error_msg': str(e)})

    # Render the template
    return render(request, template, render_dict)


def choose_sequence(request, qset):
    """
    :param request:
    :param qset:

    Receives original request and QuerySet with all sequences possible. Create a
    page with each of them as a choice and pointing to the same page but with
    the sequence_id parameter set."""
    del request, qset

    print('NOT IMPLEMETED!')
    sys.exit(1)


def register_event(user_id, sequence_id, stage_number, operation=-1):
    """
    :param user_id:
    :param sequence_id:
    :param stage_number:
    :param operation:

    Function that will eventually send an event to a tracking mechanism
    """
    del user_id, sequence_id, stage_number, operation

    pass


def process_problem_url(url, key):
    """
    :param url: Points to the problem
    :param key: Key value to remove

    Parse the URL and:
    - remove the pair key=value from the query string
    - Detect if the url has the "instantiate" subpath, and if so, add the seed
    """

    rseed = datetime.now().microsecond
    parsed = urlparse(url)
    qd = parse_qs(parsed.query, keep_blank_values=True)
    filtered = dict((k, v) for k, v in list(qd.items()) if not k == key)
    if exco_instantiate_url in parsed.path:
        filtered['rseed'] = rseed

    return urlunparse([parsed.scheme,
                       parsed.netloc,
                       parsed.path,
                       parsed.params,
                       urlencode(filtered, doseq=True),
                       parsed.fragment]), rseed


def load_exercise_module(module_dir, class_name, rseed):
    if not os.path.isdir(module_dir):
        raise Exception('%s does not contain a python module.' %
                        module_dir)

    # Extract the path to add to system path and make the load work
    (module_dir, module_suffix) = os.path.split(os.path.abspath(module_dir))

    # Insert dirname in the path to load modules
    if module_dir not in sys.path:
        sys.path.insert(0, module_dir)

    # Load the given module
    try:
        ctx_handler = __import__(module_suffix)
    except ImportError as e:
        print(e)
        raise Exception('Unable to import %s/%s. Terminating' % (module_dir,
                                                                 module_suffix))

    # We need to call one single method!
    try:
        handler_class = getattr(ctx_handler, class_name)
    except AttributeError:
        return None

    return handler_class(rseed).render_dict
