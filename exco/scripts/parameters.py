#
# File containing execution parameters.
#
# It is not supposed to be executed but read by other scripts
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the Reauthoring toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function


class ParametersBase(object):

    debug = False

    sequence_id = ''
    sequence_desc = ''

    # Use Glob
    exercise_files = ''
    url_prefix = ''

    # Home link and text
    home_url = '.'
    home_text = ''

    # Number of choices (default 4)
    number_of_choices = 4

    # Username
    username = ''

    # Context
    context = ''

    # Number of correct answers to consider the sequence done
    max_score = -1

    def __init__(self):
        return

    def print_data(self):
        """
        Print the value of the variables
        :return:
        """

        print()
        print('Debug:', self.debug)
        print('Sequence_id:', self.sequence_id)
        print('Sequence_desc:', self.sequence_desc)
        print('Exercise_files:', self.exercise_files)
        print('Url_prefix:', self.url_prefix)
        print('Home URL:', self.home_url)
        print('Home text', self.home_text)
        print('Number of choices:', self.number_of_choices)
        print('Context:', self.context)
        print()
