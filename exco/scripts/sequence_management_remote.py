#!/usr/bin/python
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the Reauthoring toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import getopt
import os
import sys

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "data2u.settings")
django.setup()

from exco.models import ProblemSequence, SequenceStatus
from user_mgmt.models import D2User, Context
import user_mgmt.user_operations

__doc__ = """
  Script that given:
  
  - Sequence name
  - Sequence description
  - Set of files with problems
  - URL prefix from where they are served
  - Link home
  - Text to link home
  - Max_Score (number of files to be considered)
  - Number of choices
  - Command name

  Executes one of the following commands:

  - create_users: Create a set of users in the platform (needed to perform
    the rest of operations in this script). The users are taken from the
    variable 'user_list' in the file given in the parameter variable
    'student_file'

  - create_sequence: Asks for information about a sequence and inserts it in
    the server. No users are enabled at this point.

  - enable_sequence: Asks for the sequence id and user id (email) and
    enables the sequence for the given user.

  - enable_sequence_all_users: Asks for a sequence-id and enables all users to 
    use that sequence. The users ar taken from the variable 'user_list' in the
    file given in the parameter variable 'student_file'
    """


def create_sequence(options):

    sequence_id = options.get('sequence_id')
    sequence_desc = options.get('sequence_desc')
    stage_urls = options.get('stage_urls')
    url_home = options.get('url_home')
    url_home_text = options.get('url_home_text')
    max_score = options.get('max_score')
    number_of_choices = options.get('number_of_choices')

    if sequence_id is None or \
       sequence_desc is None or \
       stage_urls is None or \
       url_home is None or \
       url_home_text is None:
        print('ERROR: Incorrect parameters:')
        for k, v in list(options.items()):
            print(k, ':', v)
        sys.exit(1)

    # Get the sequence from the Database 
    p_sequence = ProblemSequence.objects.filter(name=sequence_id)

    if len(p_sequence) == 0:
        print('Creating new sequence.')
        p_sequence = ProblemSequence(name=sequence_id,
                                     description=sequence_desc,
                                     stage_urls=stage_urls,
                                     url_home=url_home,
                                     url_home_text=url_home_text,
                                     max_score=max_score,
                                     number_of_choices=number_of_choices)
    else:
        print('Sequence already exists: updating')
        p_sequence = p_sequence[0]
        p_sequence.sequence_desc = sequence_desc
        p_sequence.stage_urls = stage_urls
        p_sequence.url_home = url_home
        p_sequence.url_home_text = url_home_text
        p_sequence.max_score = max_score
        p_sequence.number_of_choices = number_of_choices

    p_sequence.save()
    return


def create_users(options):
    """
    Create the users in the comma separated list of users with key 'user_list'
    in the options dict passed as parameter.
    """

    user_list = options.get('user_list')
    if user_list is None:
        print('ERROR: Incorrect parameters:')
        for k, v in list(options.items()):
            print(k, ':', v)
        sys.exit(1)

    for username in user_list:
        if user_mgmt.user_operations.user_lookup_by_email(username) is None:
            print('Creating user account', username)
            user_mgmt.user_operations.activate_account(username)
        else:
            print('User', username, 'already exists. Nothing done')


def enable_sequence(options):
    """
    Enable the sequence with id in options['sequence_id'] for the users in the
    comma separated list of users in options['user_list'].
    """

    debug = options.get('debug')
    sequence_ids = options.get('sequence_list')
    user_list = options.get('user_list')
    if sequence_ids == [] or \
       user_list is None:
        print('ERROR: Incorrect parameters:')
        for k, v in list(options.items()):
            print(k, ':', v)
        sys.exit(1)

    for sequence_id in sequence_ids:
        for username in user_list:
            # Check if the sequence exists
            sequence = ProblemSequence.objects.filter(name=sequence_id)

            if len(sequence) == 0:
                print('ERROR: Sequence', sequence_id, end=' ')
                print('does not exist. Insert first.')
                sys.exit(1)
            sequence = sequence[0]

            # Check if the user exists
            user = D2User.objects.filter(email=username)

            if len(user) == 0:
                print('ERROR: User', username, \
                    'does not exist. Insert first.')
                sys.exit(1)
            user = user[0]

            # Check if the entry is present
            if debug:
                print('Checking if sequence', sequence.name, end=' ')
                print('is enabled for user', user.email)

            seq_status = SequenceStatus.objects.filter(
                user__email=user.email,
                problem_sequence=sequence,
                current=True)

            if len(seq_status) != 0:
                print('WARNING: User', user.email, 'already has sequence', end=' ')
                print(sequence.name, 'enabled')
                continue

            # Insert the new instance of the sequence
            if debug:
                print('Creating sequence', sequence.name, 'for user', \
                    user.email)

            new_sequence = SequenceStatus(
                user=user,
                problem_sequence=sequence,
                current=True)
            new_sequence.save()
        

def enable_sequence_in_context(options):
    """
    Enable the sequence with id in options['sequence_id'] for the users that
    are members of the context in options['context']
    """

    debug = options.get('debug')
    sequence_ids = options.get('sequence_list')
    context = options.get('context')
    if sequence_ids == [] or context is None:
        print('ERROR: Incorrect parameters:')
        for k, v in list(options.items()):
            print(k, ':', v)
        sys.exit(1)

    # Get the list of user_ids that are context members
    user_list = Context.objects.filter(
        name=context).values_list('members', flat=True)

    for sequence_id in sequence_ids:
        for user_id in user_list:
            # Check if the sequence exists
            sequence = ProblemSequence.objects.filter(name=sequence_id)

            if len(sequence) == 0:
                print('ERROR: Sequence', sequence_id, end=' ')
                print('does not exist. Insert first.')
                sys.exit(1)
            sequence = sequence[0]

            # Check if the user exists
            user = D2User.objects.filter(id=user_id)

            if len(user) == 0:
                print('ERROR: User', user_id, 'does not exist. Insert first.')
                sys.exit(1)
            user = user[0]

            # Check if the entry is present
            if debug:
                print('Checking if sequence', sequence.name, end=' ')
                print('is enabled for user', user.email)

            seq_status = SequenceStatus.objects.filter(
                user__email=user.email,
                problem_sequence=sequence,
                current=True)

            if len(seq_status) != 0:
                if debug:
                    print('WARNING: User', user.email, 'already has sequence', end=' ')
                    print(sequence.name, 'enabled')
                continue

            # Insert the new instance of the sequence
            if debug:
                print('Creating sequence', sequence.name, \
                    'for user', user.email)

            new_sequence = SequenceStatus(
                user=user,
                problem_sequence=sequence,
                current=True)
            new_sequence.save()

# Execution as script
if __name__ == "__main__":

    # Default options
    options = {
        'debug': False,
        'sequence_id': None,
        'sequence_desc': None,
        'stage_urls': None,
        'url_home': None,
        'url_home_text': None,
        'max_score': None,
        'number_of_choices': None,
        'user_list': None,
        'context': None
        }

    # Swallow the options
    try:
        opts, args = getopt.getopt(sys.argv[1:], 
                                   "di:I:e:u:h:t:s:c:l:x:",
                                   [])
    except getopt.GetoptError:
        print('Incorrect option.', file=sys.stderr)
        print(__doc__, file=sys.stderr)
        sys.exit(2)

    # Parse the options
    for optstr, value in opts:
        # Debug option
        if optstr == "-d":
            options['debug'] = True
        elif optstr == "-i":
            options['sequence_id'] = value
        elif optstr == "-I":
            options['sequence_list'] = value.split(',')
        elif optstr == "-e":
            options['sequence_desc'] = value
        elif optstr == "-u":
            options['stage_urls'] = value
        elif optstr == "-h":
            options['url_home'] = value
        elif optstr == "-t":
            options['url_home_text'] = value
        elif optstr == "-s":
            options['max_score'] = int(value)
        elif optstr == "-c":
            options['number_of_choices'] = int(value)
        elif optstr == "-l":
            options['user_list'] = value.split(',')
        elif optstr == "-x":
            options['context'] = value
        else:
            print('ERROR: Option', optstr, 'not valid.')
            sys.exit(1)

    # Scripts need a command
    if len(args) != 1:
        print('Script needs a command name.')
        print(__doc__)
        sys.exit(1)

    # Dump options
    if options['debug']:
        for k, v in list(options.items()):
            print(k, ':', v)

    if args[0] == 'create_sequence':
        create_sequence(options)
    elif args[0] == 'create_users':
        create_users(options)
    elif args[0] == 'enable_sequence':
        enable_sequence(options)
    elif args[0] == 'enable_sequence_in_context':
        enable_sequence_in_context(options)
    else:
        print('ERROR: Command', args[0], 'not allowed.')
