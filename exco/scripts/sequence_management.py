#
# Copyright (C) 2014 The University of Sydney
# This file is part of the Reauthoring toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import getopt
import importlib
import os
import re
import shlex
import sys

from exco.models import ProblemSequence, SequenceStatus
from user_mgmt.models import Context
import user_mgmt.user_operations


# Inserting current dir as first argument
sys.path.insert(0, os.getcwd())

__doc__ = """

Script to handle sequences in a remote server. The script needs one single
parameter with the command to execute out of the following:

  - create_sequence: Create a sequence in the database. The information about
    the sequence is obtained from the following elements in the 'parameters'
    object:

    - 'sequence_id': Sequence name
    - 'sequence_desc': Sequence description
    - 'exercise_files': Set of files with problems
    - 'url_prefix': URL prefix from where they are served
    - 'home_url: Link to the course home
    - 'home_text': Text to use for the link to the course home
    - 'number_of_choices': Number of choices per question
    - 'context': Name of the context

  - enable_sequence: Asks for the sequence id and user id (email) and
    enables the sequence for the given user.

  - enable_sequence_all_users: Asks for a sequence-id and enables all users to
    use that sequence. The users ar taken from the variable 'user_list' in the
    file given in the parameter variable 'student_file'

  Examples:

  - manage.py runscript exco.scripts.sequence_management \
              -script-arg=enable_sequence_all_users

  - manage.py runscript exco.scripts.sequence_management \
              -script-arg=create_sequence

  Author: Abelardo Pardo <abelardo.pardo@sydney.edu.au>
"""

# Insert the current dir in the python path to pick up the config file name.
sys.path.insert(0, os.getcwd())


def get_sequence_ids(parameters):
    """
    Function that checks the values of sequence_id and sequence_list in the
    parameter object and requests the value from the user if needed.
    :return: Values are reflected in the parameters global object.
    """

    # If both sequence_id and list are empty, we ask for a single sequence
    #  name
    if parameters.sequence_id == '' and parameters.sequence_list == []:
        print("Sequence ID:", end=' ')
        parameters.sequence_list = [sys.stdin.readline()[:-1]]
    else:
        # If there is a sequence id, we give it priority
        if parameters.sequence_id != '':
            parameters.sequence_list = [parameters.sequence_id]


def build_exercise_urls(url_prefix, files):
    """
    Function that given a url and a regular expression returns a set of files.
    """

    if not files:
        print('ERROR: No files provided to script')
        sys.exit(1)

    url_suffix = []
    for filename in files:
        with open(filename) as f:
            state = 0
            for line in f:
                if state == 0 and re.match('\.\. iguide:: Solution', line):
                    state = 1
                elif state == 1:
                    state = 2
                elif state == 2:
                    # Detected the solution, skip
                    try:
                        url_suffix.append('?excosol=' + str(int(line)))
                    except ValueError:
                        print('Line', line, 'in file', filename, end=' ')
                        print('has incorrect format')
                        sys.exit(1)
                    break
            # If the exercise does not have a solution line, stick a -1 (it
            # is a parametric exercise)
            if state != 2:
                url_suffix.append('?excosol=-1')

    return [url_prefix + '/' + x[0].replace('.rst', '.html') + x[1]
            for x in zip(files, url_suffix)]


def create_sequence(parameters):
    """Function that gets:
    
    - Sequence name
    - Sequence description
    - Set of files with problems
    - URL prefix from where they are served
    - Link home
    - Text to link home
    - Score (number of files to be considered)
    - Number of choices
    
    And creates or updates its value in the platform.
    """

    # Read the information from command line if they are not given.
    if parameters.sequence_id == '':
        print("Sequence ID:", end=' ')
        parameters.sequence_id = sys.stdin.readline()[:-1]

    if parameters.sequence_desc == '':
        print("Sequence Description: ", end=' ')
        parameters.sequence_desc = sys.stdin.readline()[:-1]
    
    if parameters.sequence_id == '' or parameters.sequence_desc == '':
        print('Both sequence ID and description must be non empty')
        sys.exit(1)

    # Read exercise files (glob)
    if parameters.exercise_files == '':
        print('Exercise files (glob): ', end=' ')
        parameters.exercise_files = sys.stdin.readline()[:-1]

    # URL to use as prefix 
    if parameters.url_prefix == '':
        print("URL Prefix: ", end=' ')
        parameters.url_prefix = sys.stdin.readline()[:-1]

    # Transform the exercise_files in a set of URLs
    parameters.exercise_files = build_exercise_urls(parameters.url_prefix,
                                                    parameters.exercise_files)

    # Get the maximum score fore the sequence (or by default th number of
    # problems in the sequence.
    max_score = parameters.max_score
    if parameters.max_score == -1:
        max_score = len(parameters.exercise_files)
    # Concatenate all strings in the list separated by spaces
    parameters.exercise_files = ' '.join(parameters.exercise_files)

    if parameters.debug:
        print('CREATING SEQUENCE:')
        print('    Sequence ID:', parameters.sequence_id)
        print('    Sequence DESC:', parameters.sequence_desc)
        print('    Exercises:', parameters.exercise_files)
        print('    Home URL:', parameters.home_url)
        print('    Home Text:', parameters.home_text)
        print('    Number of choices:', parameters.number_of_choices)
 
    # Get the sequence from the Database
    p_sequence = ProblemSequence.objects.filter(name=parameters.sequence_id)

    # Sequence does not exist. Create a new one.
    if not p_sequence:
        print('Creating new sequence.', file=sys.stderr)
        p_sequence = ProblemSequence()
        ctx = Context.objects.get(name=parameters.context)
        p_sequence.context = ctx
        p_sequence.name = parameters.sequence_id
    else:
        # Sequence already exists, but it has to be updated
        if parameters.debug:
            print('Sequence already exists: updating', file=sys.stderr)
        # Get the first element of the QuerySet
        p_sequence = p_sequence[0]

    # Set the proper values in the object
    p_sequence.description = parameters.sequence_desc
    p_sequence.stage_urls = parameters.exercise_files
    p_sequence.url_home = parameters.home_url
    p_sequence.url_home_text = parameters.home_text
    p_sequence.max_score = max_score
    p_sequence.number_of_choices = parameters.number_of_choices

    p_sequence.save()


def enable_sequence(parameters, user_list=None):
    """
    Given a user and a sequence name inserts a new sequence_status if one
    doesn't exist for the given user and sequence name.
    """

    # Read the information from command line so that we postpone the code in
    # the server to the end.

    # Get the list of sequence ids
    get_sequence_ids(parameters)

    # Read username and set it in the list of users to process
    if not user_list:
        print("Username: ", end=' ')
        user_list = [sys.stdin.readline()[:-1]]

    user_objs = []
    for username in user_list:
        # Check if the user exists
        user = user_mgmt.user_operations.user_lookup_by_email(username)
        if not user:
            print('ERROR: User', parameters.username, 'does not exist.')
            print('Insert the user as member of the context.')
            sys.exit(1)

        user_objs.append(user)

    # Enable the sequence for all those objects
    enable_sequence_user_objs(parameters, user_objs)


def enable_sequence_user_objs(parameters, user_objs):
    """

    :param parameters: Set of parameters given for the execution
    :param user_objs: List of user objects
    :return: Side effect. Enable the sequence for all user objects in param
    """

    # Loop over the given sequences
    for sequence_id in parameters.sequence_list:
        for user in user_objs:

            # Check if the sequence exists
            sequence = ProblemSequence.objects.filter(name=sequence_id)

            if not sequence:
                print('ERROR: Sequence', sequence_id,
                      'does not exist. Insert the sequene first.')
                sys.exit(1)
            sequence = sequence[0]

            # Check if the entry is present
            if parameters.debug:
                print('Checking if sequence', sequence.name,
                      'is enabled for user', user.email)

            seq_status = SequenceStatus.objects.filter(
                user__email=user.email,
                problem_sequence=sequence,
                current=True)

            if seq_status:
                print('WARNING: User', user.email, 'already has sequence',
                      sequence.name, 'enabled. Ignoring.')
                continue

            # Insert the new instance of the sequence
            if parameters.debug:
                print('Creating sequence', sequence.name, 'for user',
                      user.email)

            new_sequence = SequenceStatus(
                user=user,
                problem_sequence=sequence,
                current=True)
            new_sequence.save()


def enable_sequence_all_users(parameters):
    
    # Enable the sequence for all users in the given context

    # If both sequence_id and list are empty, we ask for a single sequence
    # name

    # Get the list of sequence ids
    get_sequence_ids(parameters)

    # Get the list of students from the given context
    users = Context.objects.get(
        name=parameters.context
    ).members.all()

    if parameters.debug:
        print('Enabling sequence for', len(users), 'users')
    
    parameters.username = None

    enable_sequence_user_objs(parameters, users)


# Execution as script
def run(*script_args):
    """

    :param script_args: Parameters given to the script to run
    :return:
    """

    # Scripts need a command
    if script_args is ():
        print('Script needs arguments.')
        print(__doc__)
        sys.exit(1)

    # Parse the arguments
    argv = shlex.split(script_args[0])

    # Default options
    debug = False
    userlist = []
    parameters = None

    # Import parameters

    # Swallow the options
    try:
        opts, args = getopt.getopt(argv, "p:u:", [])
    except getopt.GetoptError:
        print('Incorrect option.', file=sys.stderr)
        print(__doc__, file=sys.stderr)
        sys.exit(2)

    # Parse the options
    for optstr, value in opts:
        # Debug option
        if optstr == "-p":
            parameters = importlib.import_module(value).Parameters()
        elif optstr == "-u":
            userlist.append(value)

    # Scripts need a command
    if len(args) != 1:
        print('Script needs a command name.')
        print('Args:', argv)
        print(__doc__)
        sys.exit(1)

    if parameters.debug:
        parameters.print_data()

    if args[0] == 'create_sequence':
        if debug:
            print('Executing create_sequence', file=sys.stderr)
        create_sequence(parameters)

    elif args[0] == 'enable_sequence':
        if debug:
            print('Executing enable_sequence', file=sys.stderr)

        if userlist:
            # If a list of users has been given, use it
            enable_sequence(parameters, userlist)
        elif parameters.username != '':
            # If no user list given and username is in parameters, use it
            enable_sequence(parameters, [parameters.username])
        else:
            # Run with no list of users (stdin)
            enable_sequence(parameters)

    elif args[0] == 'enable_sequence_all_users':
        if debug:
            print('Executing enable_sequence_all_users', file=sys.stderr)
        parameters.username = None
        enable_sequence_all_users(parameters)
    else:
        print('ERROR: Command', args[0], 'not allowed.')
