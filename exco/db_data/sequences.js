// conn = new Mongo();
// db = conn.getDB("test");

db.problem_sequence.remove()
db.problem_sequence.insert(
    { "_id" : "aaa",
      "number_of_choices" : 4,
      "stage_urls" : ["http://127.0.0.1:8000/static/problem_1.html?excosol=0",
                      "http://127.0.0.1:8000/static/problem_2.html?excosol=1",
                      "http://127.0.0.1:8000/static/problem_3.html?excosol=2",
                      "http://127.0.0.1:8000/static/problem_4.html?excosol=3",
                      "http://127.0.0.1:8000/static/problem_5.html?excosol=0",
                      "http://127.0.0.1:8000/static/problem_6.html?excosol=1",
                      "http://127.0.0.1:8000/static/problem_7.html?excosol=2",
                      "http://127.0.0.1:8000/static/problem_8.html?excosol=3",
                      "http://127.0.0.1:8000/static/problem_9.html?excosol=0",
                      "http://127.0.0.1:8000/static/problem_10.html?excosol=1"],
      "stage_max_scores" : [ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ]
    }
)
db.problem_sequence.insert(
    { "_id" : "bbb",
      "number_of_choices" : 5,
      "stage_urls" : ["http://127.0.0.1:8000/static/problem_1.html?excosol=0",
                      "http://127.0.0.1:8000/static/problem_2.html?excosol=1",
                      "http://127.0.0.1:8000/static/problem_3.html?excosol=2",
                      "http://127.0.0.1:8000/static/problem_4.html?excosol=3",
                      "http://127.0.0.1:8000/static/problem_5.html?excosol=4",
                      "http://127.0.0.1:8000/static/problem_6.html?excosol=0",
                      "http://127.0.0.1:8000/static/problem_7.html?excosol=1",
                      "http://127.0.0.1:8000/static/problem_8.html?excosol=2",
                      "http://127.0.0.1:8000/static/problem_9.html?excosol=3",
                      "http://127.0.0.1:8000/static/problem_10.html?excosol=4" ],
      "stage_max_scores" : [ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ]
    }
)
db.problem_sequence.insert(
    { "_id" : "ccc",
      "number_of_choices" : 5,
      "stage_urls" : ["http://127.0.0.1:8000/static/problem_1.html?excosol=0",
                      "http://127.0.0.1:8000/static/problem_2.html?excosol=1" ],
      "stage_max_scores" : [ 1, 1 ]
    }
)

// ------------------------------------------------------------------------------

db.sequence_status.insert(
    { "user_id" : "abel",
      "problem_sequence_id" : "aaa",
      "correct" : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      "incorrect" : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      "attempts" : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      "score" : 0,
      "start_time":
    }
)

