#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import random


class ExerciseGenerator(object):
    # Number of render_dicts to keep in the cache
    cache_size = 200
    # Dict to map (class_name, seed) -> render_dict
    dict_cache = {}
    # Keys in the dict_cache dictionary to detect which one to remove next
    key_list = []
    # Turn on/off debugging of the problems
    debug = False

    # Attributes to be overwritten
    render_dict = None
    rnd = None
    seed = None

    def __init__(self, seed):
        # Initialise exercise and return the dictionary from the cache
        self.cache_hit = None
        self.render_dict = self.exercise_prelude(seed)

    def shuffle_and_pick(self, value_list, pick):
        """
        Given a list, shuffles its values (in place) and returns the index of
        the element given as "pick"
        :param value_list: List of values to shuffle
        :param pick: Value to detect after shuffling
        :return: index of the "pick" in the resulting list
        """
        self.rnd.shuffle(value_list)
        return value_list.index(pick)

    def shuffle_and_pick_except_last(self, value_list, pick):
        """
        Removes the last element, shuffles the remaining list, and returns
        the index of the pick element.

        :param value_list: List to shuffle (except last element)
        :param pick: Element to detect after shuffling
        :return: index of the "Pick" in the resulting file
        """
        result = value_list[:-1]
        self.shuffle_and_pick(result, pick)
        result.append(value_list[-1])
        return result

    def insert_in_cache(self, cname, seed, render_dict):
        # See if we need to remove one element
        if len(self.key_list) == self.cache_size:
            to_remove = self.key_list.pop(0)
            self.dict_cache.pop(to_remove)

        # Insert the element in the cache
        self.dict_cache[(cname, seed)] = render_dict
        self.key_list.append((cname, seed))

        if self.debug:
            print('Cache insertion.', len(self.key_list), 'elements')

    def exercise_prelude(self, seed):
        """
        :param seed: Seed to initalise random number generator
        :return: render_dict found in cache or None
        """
        # Get the seed and initialise the random number generator
        self.rnd = random.Random(seed)
        self.seed = seed

        # Get class name
        cname = self.__class__.__name__

        if self.debug:
            print('BEGIN', self.__class__.__name__)
            print('seed:', seed)

        cache_element = self.dict_cache.get((cname, seed), None)

        if cache_element is not None:
            # Cache HIT!!!
            if self.debug:
                print('CACHE HIT!')
                print('Render_dict:', cache_element)
                print('END', self.__class__.__name__)

            self.cache_hit = True
            return cache_element

        # Initialise the render dictionary
        self.render_dict = {}

        # If seed is not present, create one as a string (will travel in the
        # URL)
        if self.seed is None:
            self.seed = 0

        # Insert render_dict in the dict_cache
        self.insert_in_cache(cname, seed, self.render_dict)

        if self.debug:
            print('Render_dict:', self.render_dict)
            print('END', cname)

        return self.render_dict

    def exercise_postlude(self):
        pass
