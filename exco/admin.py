#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from django.contrib import admin
from exco.models import ProblemSequence, SequenceStatus


class ExcoAdminProblemSequence(admin.ModelAdmin):
    list_display = ('name', 'description', 'stage_urls', 
                    'url_home', 'url_home_text',
                    'solutions', 'max_score', 
                    'number_of_choices')
    search_fields = ['name', 'description']


class ExcoAdminSequenceStatus(admin.ModelAdmin):
    list_filter = ['current', 'user']
    search_fields = ['user__email']
    list_display = ('user', 'problem_sequence', 'score', 'last_problem', 
                    'correct', 'incorrect', 'start_time',
                    'last_access', 'finish_time', 'current')


admin.site.register(ProblemSequence, ExcoAdminProblemSequence)
admin.site.register(SequenceStatus, ExcoAdminSequenceStatus)
