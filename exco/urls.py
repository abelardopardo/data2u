#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from django.conf.urls import *

import exco.views

urlpatterns = [
    url(r'^input$', exco.views.input_sequence_form),
    url(r'^post_sequence$', exco.views.input_sequence_process),
    url(r'^sequence_step$', exco.views.sequence_step),
    url(r'^up_attempt$',    exco.views.update_attempt),
    url(r'^reset_sequence$', exco.views.reset_sequence),
    url(r'^instantiate/(?P<module_name>\w+)/(?P<exercise>\w+)\.html',
        exco.views.instantiate_exercise)
]
