#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
import re
from django.db import models
from django.conf import settings
from django.core.validators import validate_comma_separated_integer_list
from django.core.validators import RegexValidator
import user_mgmt.models


#
# Problem sequence
#
# class Problem_sequence(Document):
class ProblemSequence(models.Model):
    # name = StringField(required = True, unique = True, primary_key = True,
    #                    verbose_name = "Sequence Name")
    name = models.CharField(unique=True,
                            primary_key=True,
                            verbose_name="Sequence ID",
                            max_length=256,
                            blank=False,
                            null=False)

    # Description of the sequence
    description = models.TextField()

    # Space separated list of URLs pointing to the problems
    stage_urls = models.TextField(blank=False,
                                  verbose_name="Space separated list of URLs")

    # The URL pointing to the Course Home
    url_home = models.CharField(verbose_name="Outside URL (course?, notes?)",
                                max_length=2048)

    url_home_text = models.CharField(verbose_name="Name for Outside URL",
                                     max_length=256)

    # Comma separated list of index with the solution, it is taken from the URLs
    solutions = \
        models.CharField(
            max_length=2048,
            # validators=[validate_comma_separated_integer_list],
            validators=[
                RegexValidator(
                    re.compile('^([-+]?[0-9]+[-,])*[+-]?[0-9]+$'),
                    'Enter only integers separated by commas.',
                    'invalid')],
            verbose_name="Solutions",
            default='',
            blank=True,
            null=True)

    # stage_max_scores = ListField(IntField(min_value = 0))
    max_score = models.IntegerField(verbose_name="Maximum score")

    # number_of_choices = IntField(min_value = 2, max_value = 16)
    number_of_choices = models.IntegerField(verbose_name="Number of choices")

    # Context where the sequence belongs to
    context = models.ForeignKey(user_mgmt.models.Context,
                                blank=False,
                                null=False,
                                on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "problem sequence"


#
# Current score for the users
#
# class Sequence_status(Document):
class SequenceStatus(models.Model):
    # user_id = StringField(required = True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)

    # problem_sequence_id = ReferenceField("Problem_sequence",
    #                                      reverse_delete_rule = CASCADE)
    problem_sequence = models.ForeignKey(ProblemSequence,
                                         on_delete=models.CASCADE)

    # Index of the last problem shown (to avoid repetitions)
    last_problem = models.IntegerField(default=-1,
                                       verbose_name="Last")

    # Results so far
    correct = models.CharField(
        validators=[validate_comma_separated_integer_list],
        blank=True,
        max_length=2048)
    incorrect = models.CharField(
        validators=[validate_comma_separated_integer_list],
        blank=True,
        max_length=2048)

    # score = IntField(min_value = 0)
    score = models.FloatField(blank=True, default=0)

    start_time = models.DateTimeField(default=None, blank=True, null=True)

    last_access = models.DateTimeField(default=None,
                                       blank=True,
                                       null=True)

    finish_time = models.DateTimeField(default=None,
                                       blank=True,
                                       null=True)

    current = models.BooleanField(blank=False, default=True)

    class Meta:
        verbose_name = "sequence status"
        verbose_name_plural = "sequence statuses"
