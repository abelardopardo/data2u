#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from django import forms

import user_mgmt.user_operations


class UserForm(forms.Form):

    version = forms.ChoiceField(label="Version", required=True)

    def __init__(self, initial_option, option_values, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        
        # Choice of categories
        self.fields['version'] = forms.ChoiceField(
            widget=forms.Select,
            choices=option_values,
            required=True)
        # If initial option is given and is > 0, set it
        if int(initial_option) > 0:
            self.initial['version'] = initial_option


class DboardSuperUserForm(UserForm):

    user = forms.ChoiceField(required=True)

    def __init__(self, initial_user, initial_option, option_values, 
                 *args, **kwargs):
        super(DboardSuperUserForm, self).__init__(initial_option,
                                                  option_values,
                                                  *args, **kwargs)
  
        choices = [(x, x) for x in
                   user_mgmt.user_operations.get_all_user_names()]
        # Get all the users from the DB
        self.fields['user'] = forms.ChoiceField(
            widget=forms.Select,
            choices=choices,
            required=True)
        # Initial value only if it is empty
        if initial_user != '':
            self.initial['user'] = initial_user
