#!/usr/bin/env python
# -*- coding: UTF-8 -*-#
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import os
import sys
import csv

from . import user_operations


def upload_allocation_csv(file_name, username_suffix=None):
    """
    Function that uploads a CSV file with the following header:

    SID, Family name, Given names, Login, UOS, IL, LAB, LEC, ,TUT

    https://web.timetable.usyd.edu.au/ttLabel.jsp?labelString=ELEC1601&sessionId=2&academicYear=2013&campusId=1

    Or similar: web.timetable.usyd.edu.au

    Search for the link that says Download Student Allocations. The file is then
    used by this script to upload the students.

    """

    # If no suffix is given, use the empty string
    if username_suffix is None:
        username_suffix = ''

    with open(file_name, 'rb') as f:
        reader = csv.reader(f, dialect='excel')

        # Detect header through field "Email", "Login" or "Institutional Email"
        id_idx = -1
        while id_idx == -1:
            header = next(reader)  # Swallow the header and split by tab

            if 'Email' in header:
                id_idx = header.index('Email')
            elif 'Login' in header:
                id_idx = header.index('Login')
            elif 'Institutional email' in header:
                id_idx = header.index('Institutional email')
            else:
                print('No Email, Login or "Institutional email" field detected')
                sys.exit(1)

        for row in reader:

            username = row[id_idx] + username_suffix

            # If surrounded by double quotes, remove them
            username = username.strip('"')

            if len(username) > 30:
                print('WARNING: Username', username, 'longer than 30 chars!')

            if user_operations.user_lookup_by_email(username) is None:
                print('Creating user account', username)
                user_operations.activate_account(username)


# Execution as script
if __name__ == "__main__":

    if len(sys.argv) < 2:
        print('Upload requires a file.')
        sys.exit(-1)

    if not os.path.exists(sys.argv[1]) or not os.path.isfile(sys.argv[1]):
        print('File', sys.argv[1], 'does not exist or cannot be accessed.')
        sys.exit(-1)

    if len(sys.argv) == 2:
        print('Uploading users in', sys.argv[1])
        upload_allocation_csv(sys.argv[1])
    else:
        print('Uploading users in', sys.argv[1], 'with suffix', sys.argv[2])
        upload_allocation_csv(sys.argv[1], sys.argv[2])
