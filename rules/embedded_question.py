#!/usr/bin/env pythona
# -*- coding: utf-8 -*-
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#



import os
from docutils import nodes
from docutils.parsers.rst import directives
from docutils.parsers.rst import roles
from sphinx.util.compat import Directive

from Sphinx_ext import common


class EQuestionAnswerType(nodes.General, nodes.Element):
    pass


def visit_e_question_answer_type_node(self, node):

    # Need to check for the MODE!
    self.body.append('<img class="correct_icon"')
    self.body.append(' style="opacity: 0; margin-left: -23px;"')
    self.body.append(' src="%s"></img>' % os.path.join(node["p_to_static"],
                                                       'Correct_20x20.png'))
    self.body.append('<img class="incorrect_icon"')
    self.body.append(' style="opacity: 0; margin-left: -23px;"')
    self.body.append(' src="%s"></img>' % os.path.join(node["p_to_static"],
                                                       'Incorrect_20x20.png'))
    self.body.append('<input type="radio" name="question" value="%s" />' %
                     node["type"])


def depart_e_question_answer_type_node(self, node):
    pass


class EQuestion(nodes.General, nodes.Element):
    pass


def visit_e_question_node(self, node):

    # Need to check for the MODE!
    form_target = node["args"][0] + '_response'

    # Get the parameters
    method = node["method"]
    action = node["action"]

    self.body.append('<div class="adagio_embedded_quiz" id="%s">' % node["args"][0])
    self.body.append('<form method="%s" action="%s" target="%s">' % \
                         (method, action, form_target))
    self.body.append('<input type="hidden" name="%s"/>' % node["id_field_name"])
    self.body.append('<input type="hidden" name="%s"/>' % node["data_field_name"])
    self.body.append('</form>')
    self.body.append('<form class="adagio_embedded_quiz_questions">')

def depart_e_question_node(self, node):
    ### Need to check for the MODE!

    form_target = node["args"][0] + '_response'

    self.body.append('</form>')
    self.body.append('<div class="adagio_embedded_quiz_buttons">')
    self.body.append('<input class="adagio_quiz_button" style="display:none;" ')
    self.body.append('type="button" value="Grade" />')
    self.body.append('<input class="adagio_quiz_button" style="display:none;" ')
    self.body.append('type="button" value="Again" />')
    self.body.append('<input class="adagio_quiz_button" style="display:none;" ')
    self.body.append('type="button" value="Solutions" />')
    self.body.append('</div>')
    self.body.append('</div>')

class Embedded_question(Directive):
    """
    Directive to insert an embedded question feedback. The proposed structure is:

    .. e-question:: question-id
       :action: The url to send the result
       :id_field_name: field name to put the id
       :data_field_name: field name to put the data

       Text describing the question

       - :e-question:`C` Answer number 1 (which is correct)
       - :e-question:`I` Answer number 2 (which is incorrect)
       - :e-question:`I` Answer number 3 (which is incorrect)
       - :e-question:`I` Answer number 4 (which is incorrect)

    """

    has_content = True
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = False
    option_spec = {
        "action": directives.uri,
        "id-field-name": directives.unchanged,
        "text-field-name": directives.unchanged
        }

    def run(self):
        # Raise an error if the directive does not have contents.
        self.assert_has_content()

        config = self.state.document.settings.env.config

        if 'action' not in self.options:
            action = config["e_question_action"]
            if action == None:
                raise ValueError('Config var e_question_action must be set')
        else:
            action = self.options["action"]

        if "id-field-name" not in self.options:
            id_field_name = config["e_question_id_field_name"]
            if id_field_name == None:
                raise ValueError('Unset config var e_question_id_field_name')
        else:
            id_field_name = self.options["id-field-name"]

        if "data-field-name" not in self.options:
            data_field_name = config["e_question_data_field_name"]
            if data_field_name == None:
                raise ValueError('Unset config var e_question_data_field_name')
        else:
            data_field_name = self.options["data-field-name"]

        method = self.options.get('method', None)
        if method == None:
            method = config["e_question_submit_method"]

        result = EQuestion(args = self.arguments,
                           action = action,
                           id_field_name = id_field_name,
                           data_field_name = data_field_name,
                           method = method)

        # Parse the nested content
        self.state.nested_parse(self.content, self.content_offset, result)

        # Loop over the question children to detect which one correspond to the
        # list of answers and add some additional attributes
        answer_list_node = None
        for question_child in result.children:
            if question_child.__class__.__name__ != 'enumerated_list':
                continue

            # Try ot obtain the e_question_answer_type. If it fails, we are
            # processing a regular node so we keep processing children
            name = question_child.children[0].children[0].children[0].__class__.__name__
            if name == 'e_question_answer_type':
                answer_list_node = question_child
                break

        if answer_list_node != None:
            answer_list_node['enumtype'] = answer_list_node['enumtype'] + ' eqt_answer_list'
        else:
            raise ValueError('No anwer list found in e-question.')

        return [result]

def e_question_answer(name, rawtext, text, lineno, inliner,
                      options={}, content=[]):

    # This role has only effect when in HTML mode.
    if inliner.document.settings.env.app.builder.format != 'html':
        return

    # The value of the role can only be C or I (correct or incorrect)
    if text != 'C' and text != 'I':
        raise ValueError('Role text must be "C" or "I"')

    # Get the relative path to the static directory
    p_to_static = common.get_relative_path_to_static(inliner.document)

    return [EQuestionAnswerType(args = options,
                                type = text,
                                p_to_static = p_to_static)], []

def setup(app):

    app.add_node(EQuestion,
                 html = (visit_e_question_node,
                         depart_e_question_node),
                 latex = (visit_e_question_node,
                          depart_e_question_node),
                 text = (visit_e_question_node,
                         depart_e_question_node),
                 man = (visit_e_question_node,
                        depart_e_question_node),
                 texinfo = (visit_e_question_node,
                            depart_e_question_node))

    app.add_directive("e-question", Embedded_question)
    app.add_config_value('e_question_action', None, True)
    app.add_config_value('e_question_id_field_name', None, True)
    app.add_config_value('e_question_data_field_name', None, True)
    app.add_config_value('e_question_submit_method', 'POST', True)

    roles.register_local_role('e-question', e_question_answer)

    app.add_node(EQuestionAnswerType,
                 html = (visit_e_question_answer_type_node,
                         depart_e_question_answer_type_node),
                 latex = (visit_e_question_answer_type_node,
                          depart_e_question_answer_type_node),
                 text = (visit_e_question_answer_type_node,
                         depart_e_question_answer_type_node),
                 man = (visit_e_question_answer_type_node,
                        depart_e_question_answer_type_node),
                 texinfor = (visit_e_question_answer_type_node,
                             depart_e_question_answer_type_node))

    # app.add_config_value('embedded_question_action', None, True)
