#!/usr/bin/env python
# -*- coding: UTF-8 -*-#
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
import json
import os

from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

import accumulate.models


@login_required
def data_pack(request, context_name, pack_type):
    """
    Serve a visualisation of the requested data

    The following data packs are accepted:

    collapse_expand : To be implemented
    dboardview: To be implemented
    duration: To be implemented
    eqt: To be implemented
    exco_answer: To be implemented
    urlview: to be implemented
    video: to be implemented
    videoeqt: to be implemented
    
   """

    # Get some initial common variables
    user_id = request.user.email
    filter_user_id = user_id
    is_superuser = request.user.is_superuser

    # Get the dictionary to use when rendering the page
    # render_dict = ctx_handler.render_dict

    # The request must be a GET
    if request.method != 'GET':
        return render(
            request,
            'severe_error.html',
            {'message': 'Page invoked with incorrect method.'}
        )

    # Handle resource_id
    resource_id = request.GET.get('resource_id', None)
    if resource_id is None:
        return render(
            request,
            'severe_error.html',
            {'message': 'Page needs to include a resource_id parameter.'}
        )

    # Handle user_id as parameter
    given_user_id = request.GET.get('user_id', None)
    if given_user_id:
        if not is_superuser and user_id != given_user_id:
            return render(
                request,
                'severe_error.html',
                {'message':
                 'You are not authorised to access this information'}
            )
        filter_user_id = given_user_id

    # Handle the version
    version = request.GET.get('version', None)

    render_dict = {
        'type': pack_type,
    }

    # Obtain the first result set of indicators from the database
    indicators = accumulate.models.Indicator.objects.filter(
        context=context_name,
        user_id__email=filter_user_id,
        type=pack_type,
        resource_id=resource_id
    )

    # Filter by version if given.
    if version is not None:
        try:
            version = int(version)
        except ValueError:
            return render(
                request,
                'severe_error.html',
                {'message': 'Parameter "version" must be an integer.'}
            )

        indicators = indicators.filter(version=version)

    # Set the length of the sample in the rendering dictionary.
    render_dict['n'] = len(indicators)

    # Start assumming that all data is empty:
    some_data = False

    if pack_type == 'EVC_VIDEO':
        evc_play = 0
        evc_pause = 0
        evc_load = 0
        evc_end = 0
        evc_seconds = 0
        for indicator in indicators:
            data = json.loads(indicator.payload)
            evc_play += data[0]
            evc_pause += data[1]
            evc_load += data[2]
            evc_end += data[3]
            evc_seconds += data[4]

        render_dict['evc_play'] = evc_play
        render_dict['evc_pause'] = evc_pause
        render_dict['evc_load'] = evc_load
        render_dict['evc_end'] = evc_end
        render_dict['evc_seconds'] = evc_seconds
        some_data = evc_play != 0 or evc_pause != 0 or evc_load != 0 or \
            evc_end != 0 or evc_seconds != 0
    elif pack_type == 'EVC_MCQ':
        evc_correct = 0
        evc_incorrect = 0
        evc_show = 0
        for indicator in indicators:
            data = json.loads(indicator.payload)
            evc_correct += data[0]
            evc_incorrect += data[1]
            evc_show += data[2]
        render_dict['evc_correct'] = evc_correct
        render_dict['evc_incorrect'] = evc_incorrect
        render_dict['evc_show'] = evc_show
        some_data = evc_correct != 0 or evc_incorrect != 0 or evc_show != 0
    elif pack_type == 'EVC_COL_EXP':
        evc_col = 0
        evc_exp = 0
        for indicator in indicators:
            data = json.loads(indicator.payload)
            evc_col += data[0]
            evc_exp += data[1]
        render_dict['evc_col'] = evc_col
        render_dict['evc_exp'] = evc_exp
        some_data = evc_col != 0 or evc_exp
    elif pack_type == 'EVC_EXCO':
        results = {}
        for indicator in indicators:
            for p, r in list(json.loads(indicator.payload)[1].items()):
                # Fetch the scores for the problem. Cor=0, Inc = 0 if none.
                p_score = results.get(p, [0, 0])
                p_score[0] += r[0]
                p_score[1] += r[1]
                results[p] = p_score

        # Obtain the maximum common prefix and suffix and remove from key names
        mcp = os.path.commonprefix(list(results.keys()))
        mcs = os.path.commonprefix([x[::-1] for x in list(results.keys())])
        for k in list(results.keys()):
            results[k[len(mcp):-len(mcs)]] = results.pop(k)

        labels = []
        correct_list = []
        incorrect_list = []
        for k in sorted(results.keys()):
            labels.append(k)
            v = results[k]
            correct_list.append(v[0])
            incorrect_list.append(v[1])
        render_dict['correct_list'] = json.dumps(correct_list)
        render_dict['incorrect_list'] = json.dumps(incorrect_list)
        render_dict['labels'] = json.dumps(labels)
        some_data = len(results) != 0

    if not some_data:
        return HttpResponse('<html><body>No data available</body></html>')

    # Normal rendering
    return render(request, 'dout/dout.html', render_dict)
