#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import os
import sys
import hashlib
import base64
from Crypto import Random
from Crypto.Cipher import AES

import django.conf


class AESCipher(object):

    def __init__(self, key):
        self.bs = 32
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) %
                                                      self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]


class HandlerBasic(object):

    """
    Expected class name when deriving from this abstract class
    """
    expected_class_name = "Handler"

    """
    Name of the context to differentiate events from multiple experiences. Make
    sure your name is unique as it will not be checked at run-time
    """
    context_name = 'YOUR CONTEXT NAME HERE'

    # A pair (low, high) to restrict the indicator version to consider
    version_range = None

    def __init__(self):
        self.render_dict = {
            'template': '',
            'template_sk': '',
            'template_sr': '',
            'data_capture_url': django.conf.settings.EVENT_RECORD_URL,
            'data_capture_method': 'POST',
            'data_capture_subject_name': 'leco_subject_field',
            'data_capture_verb_name': 'leco_verb_field',
            'data_capture_object_name': 'leco_object_field',
            'data_capture_context_name': 'leco_context_field',
            'data_capture_context': 'YOUR CONTEXT NAME HERE',
            'page_title': '',
            'user_id': '',
            'no_data': True,
            'show_suggestions': False,
            'dboard_title': '',
            'suggestion_title': 'Your Learning Strategy'
        }

    def get_version(self, data_dict=None):
        """
        :param data_dict: Optional dictionary with categories

        :return: Returns one of the possible categories.
        """
        del data_dict

        return 0

    def update_render_dict(self, username, version=None):
        """
        Performs some last minute updates in the render dictionary before
        being used.
        param: username Some auxiliary parameter that may be needed
        :return:
        """
        pass


def load_module(module_dir):

    """
    :param module_dir: Full path to a python module to load."""

    if not os.path.isdir(module_dir):
        raise Exception('%s must be a directory containing a python module.' %
                        module_dir)

    # Extract the path to add to system path and make the load work
    (module_dir, module_suffix) = os.path.split(os.path.abspath(module_dir))

    # Insert dirname in the path to load modules
    if module_dir not in sys.path:
        sys.path.insert(0, module_dir)

    # Load the given module
    try:
        ctx_handler = __import__(module_suffix)
    except Exception as e:
        # Display error message
        print(e)
        print('Unable to import', module_dir, '. Terminating.')
        sys.exit(1)

    class_name = HandlerBasic.expected_class_name
    try:
        handler_class = getattr(ctx_handler, class_name)
    except AttributeError:
        print('No class with name', class_name, 'in module.')
        sys.exit(1)

    return handler_class()
